<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    protected $table = 'nivels';

    protected $fillable = ['nombre'];

    public function bibliotecas(){
        return $this->hasMany('App\Biblioteca');
    }

    public function materias(){
        return $this->hasMany('App\Materia');
    }
}
