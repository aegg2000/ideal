<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->firstName . ' ' . $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'noticia' => $faker->randomElement($array = array('checked', null)),
        'usuario' => $faker->randomElement($array = array('checked', null)),
        'agenda' => $faker->randomElement($array = array('checked', null)),
        'biblioteca' => $faker->randomElement($array = array('checked', null)),
    ];
});

$factory->define(App\Noticia::class, function (Faker\Generator $faker) {

    return [
        'titulo' => $faker->unique()->realText($maxNbChars = 70),
        'resumen' => $faker->realText($maxNbChars = 155),
        'contenido' => $faker->paragraphs($nb = 4, $asText = true),  
        'urlImg' => $faker->imageUrl($width = 640, $height = 480),
        'user_id' => $faker->numberBetween($min = 1, $max = 30),
    ];
});

$factory->define(App\Biblioteca::class, function (Faker\Generator $faker) {

	return [
		'titulo' =>   $faker->realText($maxNbChars = 70),
		'contenido' => $faker->realText($maxNbChars = 155),
		'enlace' => $faker->domainName,
		'nivel_id' => $faker->numberBetween($min = 1, $max = 11),
		'materia_id' => $faker->numberBetween($min = 1, $max = 60),
		'user_id' => $faker->numberBetween($min = 1, $max = 30),
	];
});

// $factory->define(App\Agenda::class, function (Faker\Generator $faker) {

// 	return [
// 		'titulo' =>   $faker->realText($maxNbChars = 70),
// 		'contenido' => $faker->realText($maxNbChars = 155),
// 		'fechaEvento' => $faker->date($format = 'Y-m-d'),
// 		'horaInicio' => $faker->time($format = 'H:i:s'),
// 		'horaFin' => $faker->time($format = 'H:i:s'),
// 		'user_id' => $faker->numberBetween($min = 1, $max = 30),
// 	];
// });
