@if($biblioteca->count() > 0)
						
	@foreach($biblioteca as $articulo)
		<div class="col s12">
			<section class="card-panel zona">
				<h4>{{ $articulo->titulo }}</h4>
				<p>{{ $articulo->contenido }}</p>
				<hr>
				<div class="row unrow">
					<div class="col s5">	
						<h5><a target="_blank" href="{{ $articulo->enlace }}">Descargar</a></h5>
					</div>
					<div class="col s7" style="margin: 1% 0px 0px 0px;">
						<span class="right grado teal lighten-1">{{ $articulo->materia->nombre }}</span>
						<span class="right grado blue lighten-1">{{ $articulo->nivel->nombre }}</span> 
					</div>
				</div>
			</section>
		</div>
	@endforeach

	<div id="loader" class="container center-align">

		@if($biblioteca->hasMorePages())
			<a id="vermas" class="vermas" href="{{ $biblioteca->nextPageUrl() }}">Cargar más artículos</a>
		@else
			<p class="blue-text">No hay más artículos</p>
		@endif

	</div>

@else
	<h2 class="centrado-mensaje">No hay artículos en esta materia</h2>
	
@endif