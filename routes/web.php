<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'FrontWebController@inicio')->name('inicio');
Route::get('inicio', 'FrontWebController@inicio')->name('inicio');
Route::get('noticia/{slug}', 'FrontWebController@noticia')->name('noticia');
Route::get('zonaideal', 'FrontWebController@zonaIdeal')->name('zonaIdeal');
Route::post('documentos', 'FrontWebController@solicitudDocumentos')->name('documentos');


Route::get('nosotros', function () {
    return view('nosotros');
});

Route::get('contacto', function () {
    return view('contacto');
});

Route::middleware('auth')->prefix('panel')->group(function(){
    Route::get('/', function(){
        return view('panel.panel');
    })->name('panel.inicio');

    Route::middleware('admUsuarios')->group(function(){
        Route::resource('usuarios', 'UsersController', ['except' => [ 'show', 'destroy' ]]);
        Route::get('usuarios/{id}/eliminar', 'UsersController@destroy')->name('usuarios.destroy');

        Route::resource('slider', 'SliderController', ['only' => ['index', 'edit', 'update']]);
    });

    Route::middleware('admNoticias')->group(function(){
        Route::resource('noticias', 'NoticiasController', ['except' => [ 'show', 'destroy' ]]);
        Route::get('noticias/{id}/eliminar', 'NoticiasController@destroy')->name('noticias.destroy');
    });

    Route::middleware('admAgenda')->group(function(){
        Route::resource('agenda', 'AgendaController', ['except' => [ 'show', 'destroy' ]]);
        Route::get('agenda/{id}/eliminar', 'AgendaController@destroy')->name('agenda.destroy');
    });

    Route::middleware('admBiblioteca')->group(function(){
        Route::resource('biblioteca', 'BibliotecaController', ['except' => [ 'show', 'destroy' ]]);
        Route::get('biblioteca/{id}/eliminar', 'BibliotecaController@destroy')->name('biblioteca.destroy');
        Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'BibliotecaController@selectAjax']);

        Route::resource('materias', 'MateriasController', ['except' => [ 'show', 'destroy' ]]);
        Route::get('materias/{id}/eliminar', 'MateriasController@destroy')->name('materias.destroy');
    });

    Route::get('docs', function(){
      return view('panel.documentacion');
    })->name('docs');
});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/home', 'HomeController@index')->name('home');
