@extends('panel.layouts.main')

@section('titulo')
	Usuarios
@endsection

@section('contenido')
<div class="contenedor">
<div class="card">
	<div class="card-content light-blue accent-4">
		<span class="card-title white-text center-align">Lista de Usuarios</span>		
	</div>
	<table class="striped responsive-table">

	    <thead>
	      <tr>
			  <th></th>
	          <th>ID</th>
	          <th>Nombre</th>
	          <th>Email</th>
	          <th>Permisos</th>
	          <th>Acción</th>          
	      </tr>
	    </thead>

	    <tbody>
	    	@foreach($usuarios as $usuario)
	    		<tr>
	    			<td></td>
	    			<td>{{ $usuario->id }}</td>
	    			<td>{{ $usuario->name }}</td>
	    			<td>{{ $usuario->email }}</td>
	    			<td>
	    				<span class="hide">{!! $permisos = false !!}</span>
	    				@if($usuario->noticia == 'checked')
	    					<span class="hide">{!! $permisos = true !!}</span>
	        				<i class="fa fa-newspaper-o fa-custom2 tooltipped" data-position="top" data-delay="50" data-tooltip="Adm. Noticia"></i>
		        		@endif
		        		@if($usuario->agenda == 'checked')
		        			<span class="hide">{!! $permisos = true !!}</span>
		        			<i class="fa fa-calendar fa-custom2 tooltipped" data-position="top" data-delay="50" data-tooltip="Adm. Agenda"></i>
		        		@endif
		        		@if($usuario->biblioteca == 'checked')
		        			<span class="hide">{!! $permisos = true !!}</span>
		        			<i class="fa fa-book fa-custom2 tooltipped" data-position="top" data-delay="50" data-tooltip="Adm. Biblioteca"></i>
		        		@endif
		        		@if($usuario->usuario == 'checked')
		        			<span class="hide">{!! $permisos = true !!}</span>
		        			<i class="fa fa-users fa-custom2 tooltipped" data-position="top" data-delay="50" data-tooltip="Adm. Usuario"></i>
		        		@endif
		        		@if($permisos == false)
		        			Usuario sin permisos
		        		@endif
	    			</td>
	    			<td>
		    			<a href="{{ route('usuarios.edit', $usuario->id) }}"><i class="material-icons">mode_edit</i></a>

		    			<a href="#!" onclick="confirmar({{ $usuario->id }}, 'usuarios')"><i class="material-icons red">delete_forever</i></a>
	    			</td>
	    		</tr>
	    	@endforeach
	    </tbody>
	  </table>
      <div class="container center-align">
      	{!! $usuarios->links() !!}
      </div>
      <div class="divider"></div>
 </div>
 <br>
 <div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/usuarios/crear') }}">
      <i class="large material-icons">add</i>
    </a>
    <ul>
      <li><a href="{{ url('panel/slider') }}" class="btn-floating green"><i class="material-icons">swap_horiz</i></a></li>
    </ul>
 </div>
</div>
@endsection

@section('script')
	@include('panel.partials.sweetSure')
@endsection
