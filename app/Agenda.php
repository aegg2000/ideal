<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = 'agendas';

    protected $fillable = ['titulo', 'contenido', 'fechaEvento', 'horaInicio', 'horaFin', 'user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
