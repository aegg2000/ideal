@extends('panel.layouts.main')

@section('titulo')
	Agregar Material Biblioteca
@endsection

@section('contenido')
<div class="container">
	<div class="row card center-align">
		{!! Form::open(['route' => 'biblioteca.store', 'method' => 'POST', 'class' => 'col s12']) !!}
		<div class="row">
			<div class="input-field col s12">
				{!! Form::text('titulo', null, ['class' => 'validate input-border-color titulo-noticia', 'required']) !!}
				{!! Form::label('titulo', 'Título del Material') !!}
			</div>
			
			<br>

			<div class="col s6">
				<div class="input-field col s12">
					{!! Form::textarea('contenido', null, ['class' => 'validate materialize-textarea', 'required']) !!}
					{!! Form::label('contenido', 'Resumen del Material') !!}
				</div>
						
				<div class="input-field col s12">
					{!! Form::url('enlace', null, ['class' => 'validate', 'required']) !!}
					{!! Form::label('enlace', 'URL del Material') !!}
				</div>
			</div>
			<div class="col s6">
				<div class="input-field col s12">
					<span class="left grey-text">Nivel Escolar</span>
					{!! Form::select('nivel_id', [
						'Preescolar' 	=> ['1' => 'Preescolar'],
						'Primaria' 		=> [
							'2' => '1er Grado',
							'3' => '2do Grado',
							'4' => '3er Grado',
							'5' => '4to Grado',
							'6' => '5to Grado',
							'7' => '6to Grado',
						],
						'Bachillerato' 	=> [
							'8' => '1er Año',
							'9' => '2do Año',
							'10' => '3er Año',
							'11' => '4to Año',
							'12' => '5to Año',
						],
					], null, ['required', 'placeholder' => 'Seleccione nivel', 'class' => 'browser-default']) !!}
					
				</div>
				
				<div class="input-field col s12">
					<span class="left grey-text">Materia</span>
					{!! Form::select('materia_id', [''=>'Seleccione materia'], null, ['required', 'class' => 'browser-default']) !!}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action">Agregar Material</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('select').material_select();
	  	});

	  	$("select[name='nivel_id']").change(function(){
	      	var nivel_id = $(this).val();
	      	var token = $("input[name='_token']").val();
	      	$.ajax({
	          url: '../../panel/select-ajax',
	          method: 'POST',
	          data: {nivel_id:nivel_id, _token:token},
	          success: function(data) {
	            $("select[name='materia_id'").html('');
	            $("select[name='materia_id'").html(data.options);
	          }
	      	});
	  	});
	</script>
@endsection