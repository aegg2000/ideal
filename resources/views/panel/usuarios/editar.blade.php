@extends('panel.layouts.main')

@section('titulo')
	Editar Usuario
@endsection

@section('contenido')
<div class="contenedor">
<div class="row card center-align">
	{!! Form::open(['route' => ['usuarios.update', $usuario->id], 'method' => 'PUT', 'class' => 'col s12']) !!}
	<div class="row">
		<article id="datosUsuario" class="col s6">
			<h3>Datos de Usuario</h3>
			<br><br>
			
			<div class="input-field col s12">
				<i class="material-icons prefix">account_circle</i>
				{!! Form::text('name', $usuario->name, ['class' => 'validate input-border-color', 'required']) !!}
				{!! Form::label('name', 'Nombre y Apellido') !!}
			</div>

			<div class="input-field col s12">
				<i class="material-icons prefix">email</i>
				{!! Form::email('email', $usuario->email, ['class' => 'validate', 'required']) !!}
				{!! Form::label('email', 'Correo Electrónico') !!}
			</div>		
		</article>

		<article id="permisos" class="col s6">
			<h3>Permisos</h3>
			<div class="row">
				<div class="col s6">												
					<p>Adm. Usuario</p>
				</div>
				<div class="col s6">
					<div class="switch">
						<label>
							No
							<input type="checkbox" name="usuario" value="checked" {{ $usuario->usuario }} >
							<span class="lever"></span>
							Si
						</label>
					</div>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col s6">												
					<p>Adm. Noticias</p>
				</div>
				<div class="col s6">
					<div class="switch">
						<label>
							No
							<input type="checkbox" name="noticia" value="checked" {{ $usuario->noticia }} >
							<span class="lever"></span>
							Si
						</label>
					</div>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col s6">												
					<p>Adm. Agenda</p>
				</div>
				<div class="col s6">
					<div class="switch">
						<label>
							No
							<input type="checkbox" name="agenda" value="checked" {{ $usuario->agenda }} >
							<span class="lever"></span>
							Si
						</label>
					</div>
				</div>
			</div>
			<hr>

			<div class="row">
				<div class="col s6">												
					<p>Adm. Biblioteca</p>
				</div>
				<div class="col s6">
					<div class="switch">
						<label>
							No
							<input type="checkbox" name="biblioteca" value="checked" {{ $usuario->biblioteca }} >
							<span class="lever"></span>
							Si
						</label>
					</div>
				</div>
			</div>
			<hr>

		</article>
	</div>
	<div class="row">
		<div class="col s12">
			<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action">Editar Usuario</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/usuarios') }}">
      <i class="large material-icons">arrow_back</i>
    </a>
</div>
</div>
@endsection