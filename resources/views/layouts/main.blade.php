<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
    	<meta charset="UTF-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      {{-- Titulo de la pagina --}}
    	<title>@yield('titulo', 'IDEAL') | Instituto de Enseñanza América Latina</title>

    	<!-- Iconos de Google -->
    	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Hojas de Estilos CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?rd5re8">
        @yield('css')
        <link rel="stylesheet" href="{{ asset('css/estilo.css') }}">

    </head>
    <body>
        <header id="headerMain" class="azul_header">
            <div class="container row unrow">
              <div id="capa1" class="hide-on-med-and-down">
                <img src="{{ url('images/header/fondo_header.png') }}">
              </div>
              <div id="capa2">
              <article class="hide-on-large-only">
                    <div id="mySidenav" class="sidenav">
                      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <a name="inicio" href="{{ asset('/') }}">Inicio</a>
                        <a name="nosotros" href="{{ asset('nosotros') }}">¿Quiénes Somos?</a>
                        <a name="zonaideal" href="{{ asset('zonaideal') }}">Zona Ideal</a>
                        <a name="contacto" href="{{ asset('contacto') }}">Contacto</a>
                    </div>

                </article>
                <article id="logo" class="col l4 s12">
                    <center><a href="{{ asset('/') }}" ><img id="logoimg" class="responsive-img" src="{{ url('images/logo/logo.png') }}"></a></center>
                </article>
                <article class="col l8 hide-on-med-and-down">
                    <nav id="navHeader" class="right">
                        <ul class="right">
                            <li><a class="@yield('activeInicio')" name="inicio" href="{{ asset('/') }}">Inicio</a></li>
                            <li><a class="@yield('activeNosotros')" name="nosotros" href="{{ asset('nosotros') }}">¿Quiénes Somos?</a></li>
                            <li><a class="@yield('activeZona')" name="zonaideal" href="{{ asset('zonaideal') }}">Zona Ideal</a></li>
                            <li><a class="@yield('activeContacto')" name="contacto" href="{{ asset('contacto') }}">Contacto</a></li>
                        </ul>
                    </nav>
                    <section id="RRSS" class="right col s4 hide-custom">
                        <a target="_blank" href="https://twitter.com/UE_IDEAL"><i class="socicon-twitter social"></i></a>
                        <a target="_blank" href="https://www.instagram.com/ue_ideal/"><i class="socicon-instagram social"></i></a>
                    </section>
                </article>
              </div>
            </div>
        </header>

        <section>
        	@yield('contenido')
        </section>

        <footer class="azul_header">
            <article class="container">
                <div class="row unrow">
                    <p id="footerP" class="col s12 center-align">Instituto de Enseñanza América Latina (IDEAL)</p>
                </div>
            </article>
        </footer>

        <div class="fixed-action-btn-left hide-on-large-only">
            <a onclick="openNav()" class="btn-floating btn blue">
              <i class="large material-icons">menu</i>
            </a>
        </div>

    	<!-- scripts -->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
        </script>
        @yield('script')
    </body>
</html>
