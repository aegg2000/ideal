@extends('panel.layouts.main')

@section('titulo')
	Nueva Materias
@endsection

@section('contenido')
<div class="contenedor">
<div class="row card center-align">
	{!! Form::open(['route' => 'materias.store', 'method' => 'POST', 'class' => 'col s12']) !!}
	<div class="row">
		<article class="col s12">
			<h3>Agregar Nueva Materia</h3>
			
			<div class="input-field col s12 m12 l6">
				{!! Form::text('nombre', null, ['class' => 'validate input-border-color', 'required']) !!}
				{!! Form::label('nombre', 'Nombre de la Materia') !!}
			</div>

			<div class="input-field col s12 m12 l6">
				{!! Form::select('nivel_id', [
					'' 			=> 'Seleccione una opción',
					'Preescolar' 	=> ['1' => 'Preescolar'],
					'Primaria' 		=> [
						'2' => '1er Grado',
						'3' => '2do Grado',
						'4' => '3er Grado',
						'5' => '4to Grado',
						'6' => '5to Grado',
						'7' => '6to Grado',
					],
					'Bachillerato' 	=> [
						'8' => '1er Año',
						'9' => '2do Año',
						'10' => '3er Año',
						'11' => '4to Año',
						'12' => '5to Año',
					],
				], ['required']) !!}
				{!! Form::label('nivel_id', 'Nivel Escolar') !!}
			</div>
			<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action">Agregar Materia</button>		
		</article>
	</div>		
	{!! Form::close() !!}
</div>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/materias') }}">
      <i class="large material-icons">arrow_back</i>
    </a>
</div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
	    	$('select').material_select();
	  	});
	</script>
@endsection