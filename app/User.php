<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'noticia', 'agenda', 'biblioteca', 'usuario',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function noticias(){
        return $this->hasMany('App\Noticia');
    }
    public function bibliotecas(){
        return $this->hasMany('App\Biblioteca');
    }
    public function agendas(){
        return $this->hasMany('App\Agenda');
    }

    public function admUsuario(){
        return $this->usuario === 'checked';
    }

    public function admNoticia(){
        return $this->noticia === 'checked';
    }

    public function admBiblioteca(){
        return $this->biblioteca === 'checked';
    }

    public function admAgenda(){
        return $this->agenda === 'checked';
    }
}
