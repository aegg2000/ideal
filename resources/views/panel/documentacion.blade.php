@extends('panel.layouts.main')

@section('titulo')
	Documentación
@endsection

@section('contenido')
  <div class="row">
    <article class="nosotros col m10 s12">
      <section id="users" class="justify card-panel section scrollspy">
        <h3>Tipos de Usuarios</h3>
        <p>Existen seis (6) tipos de administradores para cada contenido gestionable de la página web del
        <i>Instituto de Enseñanza de América Latina</i>, los cuales se distribuyen de la siguiente manera:
        </p>
        <p>
          <b>Administrador de Usuarios:</b> cuenta con los permisos para agregar nuevos usuario, editar la información
          de algún usuario en específico o de eliminar uno o más usuarios.<br>Adicional puede gestionar el <b>Carrousel de Imágenes</b> que se halla en la sección de <i>Inicio</i> de la página principal.
        </p>
        <p>
          <b>Aministrador de Noticias:</b> gestiona las noticias asociadas al <i>Instituto de Enseñanza de América Latina</i>, agregando nuevas noticas, editando alguna infromación de una noticia específica i eliminar noticias pocos relevantes a la fecha según cierto criterio.
        </p>
        <p>
          <b>Administrador de Agenda:</b> gestiona los eventos que se estarán realizando en el <i>Instituto de Enseñanza de América Latina</i>. Puede agregar, editar algún evento o eliminarlo. Éstos eventos son visibles en el calendario que se encuentra en la sección de <i>Inicio</i> de la página principal.
        </p>
        <p>
          <b>Administrador de Biblioteca:</b> gestiona el contenido descargable que se encuentra en la sección <i>Zona Ideal</i> de la página principal. Puede anadir un nuevo material, editar alguna información o eliminar material que ya no sea pertinente como contenido descargable. También se encarga de agregar, editar y elminar las materias que se imparten en el <i>Instituto de Enseñanza de América Latina</i>.
        </p>
      </section>
    </article>
    <article class="nosotros col m10 s12">
      <section id="usuarios" class="justify card-panel section scrollspy">
        <h3>Panel de Usuarios</h3>
        <p><b>Agregar usuario:</b>
          Se presentan campos de <b>Nombre y Apellido, Correo Electrónico y Contraseña</b> que deberá suministrar según los datos del nuevo usuario.<br>
          A ese nuevo usuario se le pueden otorgar uno o más permiso de administrador, para ser un <i>Administrador único</i> o un <i>Administardor múltiple</i>,  y para ello se tiene un panel lateral derecho de selectores.<br>
          Finalmente verifíque que la información suministrada es correcta y haga click en <b>"CREAR USUARIO"</b>.
        </p>
        <img class="center materialboxed" src="{{asset('img/doc/add_user.jpg')}}" alt="" width="350px">
        <p><b>Editar información de usuario:</b>
          Se presentan los campos de <b>Nombre y Apellido, así como el Correo Electrónico</b> asociado al usuario seleccionado.
          Pueden cambiar dicha información, y añadir o quitar nuevos permisos al usuario.<br>
          Verifíque la información y haga click en <b>"EDITAR USUARIO"</b>.
        </p>
        <img class="center materialboxed" src="{{asset('img/doc/edit_user.jpg')}}" alt="" width="350px">
        <p><b>Eliminar un usuario:</b>
          En la lista de usuario podrá encontrar botones en la columna de <b>Acción</b>, siendo <i class="material-icons" style="color: blue">mode_edit</i> (Botón de Editar) y <i class="material-icons red">delete_forever</i> (Botón Eliminar) correspondientemente. Al hacer click en este último aparecerá un mensaje para verificar si desea realmente eliminar al usuario seleccionado, hacer click en <b>Cancelar</b> o <b>Borrar</b> según sea su criterio.
        </p>
        <img class="center materialboxed" src="{{asset('img/doc/delete_user.jpg')}}" alt="" width="350px">
      </section>
    </article>
    <article class="nosotros col m10 s12">
      <section id="slider" class="justify card-panel section scrollspy">
        <h3>Panel de Slider</h3>
        <p>
          Para poder acceder a éste menú posicione el puntero del mouse encima del botón Añadir en el listado de los usuarios, accediendo como un Administrador de Usuarios.
        </p>
        <img class="center materialboxed" src="{{asset('img/doc/slider_bn.jpg')}}" alt="" width="80px">
        <p>
          Podrá <b>REMOVER</b> la imagen seleccionada o hacer click en ella, para posteriormente mediante un navegador de carpetas que se le presentará, seleccionar una nueva imagen. Se recomienda que el tamaño de imagen sea de 800 x 400 px.
          <br>Finalmente hacer click en <b>ACTUALIZAR SLIDER</b>.
        </p>
        <img class="center materialboxed" src="{{asset('img/doc/slider.jpg')}}" alt="" width="350px">
      </section>
    </article>
    <article class="nosotros col m10 s12">
      <section id="noticias" class="justify card-panel section scrollspy">
        <h3>Panel de Noticias</h3>
        <p>

        </p>
      </section>
    </article>
    <article class="nosotros col m10 s12">
      <section id="biblioteca" class="justify card-panel section scrollspy">
        <h3>Panel de Biblioteca</h3>
        <p>
          
        </p>
      </section>
    </article>
    <article class="nosotros col m10 s12">
      <section id="materia" class="justify card-panel section scrollspy">
        <h3>Panel de Materias</h3>
        <p>

        </p>
      </section>
    </article>
    <article class="nosotros col m10 s12">
      <section id="agenda" class="justify card-panel section scrollspy">
        <h3>Panel de Agenda</h3>
        <p>

        </p>
      </section>
    </article>
    <article class="col m2 hide-on-small-only" style="position: fixed; left: 83%">
      <ul class="section table-of-contents">
        <li><a href="#users">Tipos de Usuarios</a></li>
        <li><a href="#usuarios">Panel Usuarios</a></li>
        <li><a href="#slider">Panel Slider</a></li>
        <li><a href="#noticias">Panel Noticias</a></li>
        <li><a href="#biblioteca">Panel Biblioteca</a></li>
        <li><a href="#materia">Panel Materia</a></li>
        <li><a href="#agenda">Panel Agenda</a></li>
      </ul>
    </article>
  </div>
  <div id="myBtn" style="display: none;" class="fixed-action-btn back-to-top">
      <a href="#"  class="btn-floating btn waves-effect waves-light blue "><i class="material-icons">arrow_upward</i></a>
    </div>
@endsection

@section('script')
  <script type="text/javascript">

    $(document).ready(function(){
      $('.scrollspy').scrollSpy();
    });

    jQuery(document).ready(function() {

    var offset = 50;
    var duration = 300;
    var duration2 = 150;

    jQuery(window).scroll(function() {

      if (jQuery(this).scrollTop() > offset) {

        jQuery('.back-to-top').animate({height: 'show'});

      } else {

        jQuery('.back-to-top').fadeOut(duration2);

      }

    });

    jQuery('.back-to-top').click(function(event) {

      event.preventDefault();
      jQuery('html, body').animate({scrollTop: 0}, duration);
      return false;

    })

  });

  </script>
@endsection
