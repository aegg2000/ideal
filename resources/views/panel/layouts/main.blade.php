</!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('titulo', 'IDEAL') | Panel de Administración</title>

	<!-- Iconos de Google -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Hojas de Estilos CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/materialize/css/materialize.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/estilo.css') }}">
		@yield('css')


</head>
<body>
    <header>
        <nav id="navpanel">
            <div class="nav-wrapper azul_header">
                    <a href="{{ url('panel') }}" class="brand-logo left"><img id="logoimg" src="{{ url('images/logo/logo.png') }}"></a>
                    <ul class="right hide-on-med-and-down">
                            <li><a class="dropdown-button" href="#!" data-activates="dropdown" data-beloworigin="true"><i class="fa fa-key fa-custom left"></i><span>Permisos</span><i class="material-icons right">arrow_drop_down</i></a></li>
                            <li><a class="dropdown-button" href="#!" data-activates="dropdown-nombre" data-beloworigin="true"><i class="material-icons left">account_circle</i><span>{{ Auth::user()->name }}</span><i class="material-icons right">arrow_drop_down</i></a></li>
                    </ul>
            </div>
        </nav>
        <!-- Dropdown Structure -->
        <ul id="dropdown-nombre" class="dropdown-content collection dropdownCustom">
                <li class="collection-item">
                        <a href="{{ route('logout') }}" class="right" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span>Cerrar Sesión</span><i class="material-icons">power_settings_new</i></a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
        </ul>

        <ul id="dropdown" class="dropdown-content collection dropdownCustom">
            @if(!is_null(Auth::user()->usuario))
                <li class="collection-item">
                        <a href="{{ url('panel/usuarios') }}" class="right"><span>Adm. Usuarios</span><i class="fa fa-users fa-custom"></i></a>
                </li>
            @endif

            @if(!is_null(Auth::user()->noticia))
                <li class="collection-item">
                    <a href="{{ url('panel/noticias') }}" class="right"><span>Adm. Noticias</span><i class="fa fa-newspaper-o fa-custom"></i></a>
                </li>
            @endif

            @if(!is_null(Auth::user()->biblioteca))
                <li class="collection-item">
                    <a href="{{ url('panel/biblioteca') }}" class="right"><span>Adm. Biblioteca</span><i class="fa fa-book fa-custom"></i></a>
                </li>
            @endif

            @if(!is_null(Auth::user()->agenda))
                <li class="collection-item">
                    <a href="{{ url('panel/agenda') }}" class="right"><span>Adm. Agenda</span><i class="fa fa-calendar fa-custom"></i></a>
                </li>
            @endif
        </ul>
    </header>

    <section>
    	@yield('contenido')
    </section>

	<!-- scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="{{ asset('plugins/materialize/js/materialize.js') }}"></script>
    <script src="{{ asset('plugins/dropify/dist/js/dropify.min.js') }}"></script>
    @include('sweet::alert')
    @include('panel.partials.sweetSure')
    @include('panel.partials.errors')
    @yield('script')

    <script type="text/javascript">
        (function($) {
            $(function() {
                $('.dropdown-button').dropdown({
                  inDuration: 300,
                  outDuration: 225,
                  hover: true, // Activate on hover
                  belowOrigin: true, // Displays dropdown below the button
                  alignment: 'right' // Displays dropdown with edge aligned to the left of button
               });
            }); // End Document Ready
        })(jQuery); // End of jQuery name space


    </script>
    </body>
</html>
