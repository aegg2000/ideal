@component('mail::message')
# **¡Su solicitud ha sido generada!**

Pronto estaremos avisandole para que retire los documentos solicitados.

**Recuerde imprimir el comprobante adjunto, firmarlo y llevarlo el dia del retiro.**

@endcomponent
