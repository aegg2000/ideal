@component('mail::message')

# ¡Nueva solicitud de documentos!

@component('mail::table')

| Representante|
| ------------- |
| Nombre: {{ $datos['representante_name'] }}|
| Cedula de identidad: {{ $datos['representante_ci'] }}|
| Numero de Telefono: {{ $datos['representante_telf'] }}|
| Correo electronico: {{ $datos['representante_email'] }}|

|Alumno|
| ------- |
| Nombre: {{ $datos['alumno_name'] }}|
| Cedula de identidad: {{ $datos['alumno_ci'] }}|
| Grado: {{ $datos['grado'] }}|

|Documentos Solicitados|
| -------  |
@if(isset($datos['cons_estudio']))
| Constancia de Estudio |
@endif
@if(isset($datos['cons_insc']))
| Constancia de Inscripción |
@endif
@if(isset($datos['cons_promo']))
| Constancia de Promoción |
@endif
@if(isset($datos['notas_certi']))
| Notas Certificadas |
@endif
@if(isset($datos['mppe']))
| Inscripción en el MPPE|
@endif
@if(isset($datos['carta_aceptacion']))
| Carta de Aceptación|
@endif
@if(isset($datos['boleta_retiro']))
| Boleta de Retiro |
@endif
@if(isset($datos['solvencia']))
| Solvencia |
@endif
@if(isset($datos['rif']))
| RIF |
@endif
@if(isset($datos['registro_mer']))
| Registro Mercantil|
@endif


|Motivo|
|------|
|{{ $datos['motivo'] }}|


@endcomponent

@endcomponent
