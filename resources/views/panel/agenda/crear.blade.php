@extends('panel.layouts.main')

@section('titulo')
	Nuevo Evento
@endsection

@section('contenido')
<div class="container">
	<div class="row card center-align">
		{!! Form::open(['route' => 'agenda.store', 'method' => 'POST', 'class' => 'col s12']) !!}
		<div class="row">
			<article id="datosEvento" class="col s12">				
				<div class="input-field col s12">
					{!! Form::text('titulo', null, ['class' => 'validate input-border-color titulo-noticia', 'required']) !!}
					{!! Form::label('titulo', 'Título del Evento') !!}
				</div>
				
				<div class="input-field col s12">
					{!! Form::textarea('contenido', null, ['class' => 'validate materialize-textarea']) !!}
					{!! Form::label('contenido', 'Resumen del Evento') !!}
				</div>

				<div class="input-field col s4">
					<input name="fechaEvento" type="text" class="datepicker" placeholder="Fecha del Evento">
				</div>

				<div class="input-field col s4">
					<input name="horaInicio" type="text" class="timepicker" placeholder="Hora Inicio del Evento">
				</div>

				<div class="input-field col s4">
					<input name="horaFin" type="text" class="timepicker" placeholder="Hora Fin del Evento">

				</div>
				<div class="input-field col s4"></div>

			</article>
		</div>
		<div class="row">
			<div class="col s12">
				<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action">Crear Evento</button>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>

@endsection

@section('script')
	<script type="text/javascript">
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 15, // Creates a dropdown of 15 years to control year,
		    today: 'Hoy',
		    clear: 'Limpiar',
		    close: 'Ok',
		    closeOnSelect: false // Close upon selecting a date,
		});

		$('.timepicker').pickatime({
		    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		    twelvehour: true, // Use AM/PM or 24-hour format
		    donetext: 'OK', // text for done-button
		    cleartext: 'Limpiar', // text for clear-button
		    canceltext: 'Cancelar', // Text for cancel-button
		    autoclose: false, // automatic close timepicker
		    ampmclickable: true, // make AM PM clickable
		    aftershow: function(){} //Function for after opening timepicker
		});
	</script>
@endsection