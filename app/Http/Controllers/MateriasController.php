<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materia;
use App\Http\Requests\MateriaRequest;

class MateriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materias = Materia::orderBy('id', 'ASC')->paginate(20);
        return view('panel.materias.index')->with('materias', $materias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.materias.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MateriaRequest $request)
    {
        $materia = new Materia($request->all());
        $materia->save();
        alert()->success('La materia <span class="green-text bold">'.$materia->nombre.'</span> de <span class="green-text bold">'.$materia->nivel->nombre.'</span> fue agregada', '¡Materia Agregada!')->html()->persistent();

        return redirect()->route('materias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $materia = Materia::find($id);
        return view('panel.materias.editar')->with('materia', $materia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MateriaRequest $request, $id)
    {
        $materia = Materia::find($id);
        $materia->fill($request->all());
        $materia->save();

        alert()->success('La materia <span class="green-text bold">'.$materia->nombre.'</span> de <span class="green-text bold">'.$materia->nivel->nombre.'</span> fue modificada', '¡Materia Actualizada!')->html()->persistent();

        return redirect()->route('materias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materia = Materia::find($id);
        $materia->delete();
        alert()->error('La Materia <span class="red-text bold">'.$materia->nombre.'</span> de <span class="red-text bold">'.$materia->nivel->nombre.'</span> fue eliminada', '¡Materia Eliminada!')->html()->persistent();
        return redirect()->route('materias.index');
    }
}
