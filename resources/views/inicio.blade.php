@extends('layouts.main')

@section('activeInicio')
	activePage
@endsection

@section('css')
	<link rel="stylesheet" href=" {{ asset('plugins/calendario/css/calendar.css') }} " />
    <link rel="stylesheet" href=" {{ asset('plugins/calendario/css/custom_2.css') }} " />
    <script src="{{ asset('plugins/calendario/js/modernizr.custom.63321.js') }}"></script>
@endsection

@section('contenido')
		<div class="slider hide-on-small-only">
	    <ul class="slides">
	      <li>
	        <img src="img/slider/slider_1.jpg">
    	  </li>
    	  <li>
	        <img src="img/slider/slider_2.jpg">
    	  </li>
    	  <li>
	        <img src="img/slider/slider_3.jpg">
    	  </li>
    	</ul>
  	</div>
    <div class="row">
	    <div class="col s12 m7 l8">
				{{-- Buscador pantallas pequeñas --}}
	    	<div class="hide-on-med-and-up">
    			{{ Form::open(['route' => 'inicio', 'method' => 'GET', 'id' => 'formBuscarMobile']) }}
	    			<div class="card-panel" style="padding: 0;">
						<div class="input-field" style="margin-top: 0;">
							<input id="searchmobile" type="search" name="titulo" placeholder="Buscar noticia">
							<label class="label-icon" for="search"><i class="material-icons">search</i></label>
							<i class="material-icons" style="margin: 2% 0;">close</i>
						</div>
	    			</div>
					{{ Form::close() }}
    		</div>

				{{-- Seccion de noticias --}}
	    	<div id="noticias" class="noticias">
	    		@include('partials.ajax-noticias')
				</div>
	    </div>

			{{-- Columna Derecha --}}
    	<div id="aside" class="col s12 m5 l4">
				{{-- Buscador pantallas medianas y grandes --}}
    		<div class="hide-on-small-only">
    			{{ Form::open(['route' => 'inicio', 'method' => 'GET', 'id' => 'formBuscar']) }}
	    			<div class="card-panel" style="padding: 0;">
						<div class="input-field" style="margin-top: 0;">
							<input id="search" type="search" name="titulo" placeholder="Buscar noticia">
							<label class="label-icon" for="search"><i class="material-icons">search</i></label>
							<i class="material-icons" style="margin: 2% 0;">close</i>
						</div>
	    			</div>
					{{ Form::close() }}
    		</div>

    		<div class="card agenda">
	    		<div class="card-content">
	    			<h4 class="center-align">Nuestra Agenda</h4>
						{{-- Calendario --}}
						<section id="calendario">
							<div class="custom-calendar-wrap">
              	<div id="custom-inner" class="custom-inner">
		              <div class="custom-header clearfix">
		              	<nav>
	                    <span id="custom-prev" class="custom-prev"></span>
	                    <span id="custom-next" class="custom-next"></span>
	                	</nav>
		                <h2 id="custom-month" class="custom-month"></h2>
		                <h3 id="custom-year" class="custom-year"></h3>
	            		</div>
	            		<div id="calendar" class="fc-calendar-container"></div>
	        			</div>
	    				</div>
						</section>
						{{-- Fecha seleccionada --}}
						<div id="fecha-click">
							<h5 class="center-align">Hoy</h5>
							@if($hoy->count() > 0)
								@foreach($hoy as $ev)
									<span>
										<div class="card-panel">
		                	<p>{{ $ev->titulo }}</p>
                    	@if(!is_null($ev->contenido))
		                  	<p class="resumen-evento black-text">{{ $ev->contenido }}</p>
											@endif
                    	@if(!is_null($ev->fechaInicio))
												<hr>
			                 	<div class="horas-evento row unrow blue-text darken-2">
			                 		<span class="col s6 center">Inicio: {{ $ev->horaInicio }}</span>
			                   	@if(!is_null($ev->fechaFin))
														<span class="col s6 center">Fin: {{ $ev->horaFin }}</span>
													@endif
			                 	</div>
											@endif
		               </div>
								 	</span>
								@endforeach
							@else
								<p class="center">No hay Eventos Hoy</p>
							@endif
						</div>
	    			</div>
    		</div>
    	</div>
    </div>
		{{-- Boton para subir --}}
		<div id="myBtn" style="display: none;" class="fixed-action-btn back-to-top">
  		<a href="#"  class="btn-floating btn waves-effect waves-light blue "><i class="material-icons">arrow_upward</i></a>
  	</div>
@endsection

@section('script')

    <script src="{{ asset('plugins/calendario/js/jquery.calendario.js') }}"></script>
	<script type="text/javascript">
		// DATA DE EVENTOS.
		var programados = {!! $json !!};
		var eventos = {
			// // 'MM-DD-YYYY' : '<span><div class="card-panel"><p> TITULO </p><p class="resumen-evento black-text">RESUMEN</p></div></span>',
			'01-01-YYYY' : '<span><div class="card-panel"><p> Año Nuevo </p></div></span>',
			'04-19-YYYY' : '<span><div class="card-panel"><p> Declaración de la Independencia </p></div></span>',
			'05-01-YYYY' : '<span><div class="card-panel"><p> Día del Trabajador </p></div></span>',
			'06-24-YYYY' : '<span><div class="card-panel"><p> Batalla de Carabobo </p></div></span>',
			'07-05-YYYY' : '<span><div class="card-panel"><p> Día de la Independencia </p></div></span>',
			'07-24-YYYY' : '<span><div class="card-panel"><p> Natalicio de Simón Bolívar </p></div></span>',
			'10-12-YYYY' : '<span><div class="card-panel"><p> Día de la Resistencia Indígena </p></div></span>',
			'12-24-YYYY' : '<span><div class="card-panel"><p> Víspera de Navidad </p></div></span>',
			'12-25-YYYY' : '<span><div class="card-panel"><p> Navidad </p></div></span>',
			'12-31-YYYY' : '<span><div class="card-panel"><p> Fiesta de Fin de Año </p></div></span>',
		};
		$.extend( true, eventos, programados );

		$(document).ready(function(){


			//SLIDER
	      	$('.slider').slider({indicators:false, height:300});

	      	// AJAX BUSCAR
	      	$(document).on('submit', '#formBuscar', function(e) {
		      	e.preventDefault();
		      	var url = $(this).attr('action')+'?titulo='+$('#search').val();
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';
		      	$('#noticias').empty();
		      	$('#noticias').html(loader);
		      	$.get(url, function(data){
		      		$('#noticias').empty();
		      		$('#noticias').append(data);
		      	});
	     	});

	     	$(document).on('submit', '#formBuscarMobile', function(e) {
		      	e.preventDefault();
		      	var url = $(this).attr('action')+'?titulo='+$('#searchmobile').val();
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';
		      	$('#noticias').empty();
		      	$('#noticias').html(loader);
		      	$.get(url, function(data){
		      		$('#noticias').empty();
		      		$('#noticias').append(data);
		      	});
	     	});

      		//AJAX NOTICIAS
		    $(document).on('click', '#vermas', function(e){
		      	e.preventDefault();
		      	var url = $(this).attr('href');
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';
		      	$('#loader').remove();
		      	$('.noticias').append(loader);
		      	$.get(url, function(data){
		      		$('.progress').remove();
		      		$('.noticias').append(data);
		      	});
	      	});

		    //BOTON BACK TO TOP
	    var offset = 750;
			var duration = 300;
			var duration2 = 150;
			jQuery(window).scroll(function() {
				if (jQuery(this).scrollTop() > offset) {
					jQuery('.back-to-top').animate({height: 'show'});
				} else {
					jQuery('.back-to-top').fadeOut(duration2);
				}
			});
			jQuery('.back-to-top').click(function(event) {
				event.preventDefault();
				jQuery('html, body').animate({scrollTop: 0}, duration);
				return false;
			});
      	});

	    //CALENDARIO JS
		$(function() {
            var transEndEventNames = {
                    'WebkitTransition' : 'webkitTransitionEnd',
                    'MozTransition' : 'transitionend',
                    'OTransition' : 'oTransitionEnd',
                    'msTransition' : 'MSTransitionEnd',
                    'transition' : 'transitionend'
                },
                transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
                $wrapper = $( '#custom-inner' ),
                $calendar = $( '#calendar' ),
                cal = $calendar.calendario({
                    onDayClick : function( $el, data, dateProperties ) {
                    	day = dateProperties.day;
						month = dateProperties.month;
						year = dateProperties.year;
						date = day+' de '+cal.getMonthName();
                        if(data.content.length > 0 ) {
                        	htmlData = '<h5 class="center-align">' + date + '</h5>';
							$.each( data.content, function( key, value ) {
								htmlData += value;
							});
                        	$('#fecha-click').empty();
                        	$('#fecha-click').html(htmlData);
                        }
                    },
                    caldata : eventos,
                    displayWeekAbbr : true,
                    events: 'click'
                }),
                $month = $( '#custom-month' ).html( cal.getMonthName() ),
                $year = $( '#custom-year' ).html( cal.getYear() );
            $( '#custom-next' ).on( 'click', function() {
                cal.gotoNextMonth( updateMonthYear );
            } );
            $( '#custom-prev' ).on( 'click', function() {
                cal.gotoPreviousMonth( updateMonthYear );
            } );
            function updateMonthYear() {
                $month.html( cal.getMonthName() );
                $year.html( cal.getYear() );
            }
        });
	</script>
@endsection
