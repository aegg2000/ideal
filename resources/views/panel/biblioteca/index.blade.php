@extends('panel.layouts.main')

@section('titulo')
	Biblioteca
@endsection

@section('contenido')
<div class="contenedor">
<div class="card">
	<div class="card-content light-blue accent-4">
		<span class="card-title white-text center-align">Lista de Articulos</span>		
	</div>
	<table class="striped responsive-table">

	    <thead>
	      <tr>
			  <th></th>
	          <th>ID</th>
	          <th>Titulo</th>
	          <th>Enlace</th>
	          <th>Nivel</th>
	          <th>Materia</th>
	          <th>Publicado Por</th>
	          <th>Acción</th>
	      </tr>
	    </thead>

	    <tbody>
	    	@foreach($materiales as $material)
	    		<tr>
	    			<td></td>
	    			<td>{{ $material->id }}</td>
	    			<td>{{ $material->titulo }}</td>
	    			<td>{{ $material->enlace }}</td>
	    			<td>{{ $material->nivel->nombre }}</td>
	    			<td>{{ $material->materia->nombre }}</td>
	    			<td>{{ $material->user->name }}</td>
	    			<td>
		    			<a href="{{ route('biblioteca.edit', $material->id) }}"><i class="material-icons">mode_edit</i></a>

		    			<a href="#!" onclick="confirmar({{ $material->id }}, 'biblioteca')"><i class="material-icons red">delete_forever</i></a>
	    			</td>
	    		</tr>
	    	@endforeach
	    </tbody>
	  </table>
      <div class="container center-align">
      	{!! $materiales->links() !!}
      </div>
      <div class="divider"></div>
 </div>
 <br>
 <div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/biblioteca/crear') }}">
      <i class="large material-icons">add</i>
    </a>
    <ul>
      <li><a href="{{ url('panel/materias') }}" class="btn-floating green"><i class="material-icons">swap_horiz</i></a></li>
    </ul>
 </div>
</div>
@endsection

@section('script')
	@include('panel.partials.sweetSure')
@endsection