<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MateriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'    =>  'required|unique_with:materias,nivel_id',
            'nivel_id'  =>  'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.unique_with' => 'Esta Materia ya se encuentra en la base de datos',
            'nombre.required' => 'Ingrese un Nombre para la Materia',
            'nivel_id.required' => 'Debe seleccionar una opcion en Nivel Escolar',
        ];
    }
}
