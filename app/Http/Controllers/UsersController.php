<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::orderBy('id', 'ASC')->paginate(20);
        return view('panel.usuarios.index')->with('usuarios', $usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.usuarios.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {   

        $usuario = new User($request->all());
        $usuario->password = bcrypt($request->password);
        $usuario->save();
        alert()->success('El usuario <span class="green-text bold">'.$usuario->name.'</span> fue agegado' ,'Usuario Agregado Exitosamente')->html()->persistent();

        return redirect()->route('usuarios.index');
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        return view('panel.usuarios.editar')->with('usuario', $usuario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $usuario = User::find($id);
        $usuario->fill($request->all());
        $usuario->noticia = $request->noticia;
        $usuario->agenda = $request->agenda;
        $usuario->usuario = $request->usuario;
        $usuario->biblioteca = $request->biblioteca;
        $usuario->save();

        alert()->success('El usuario <span class="green-text bold">'.$usuario->name.'</span> fue editado' ,'¡Usuario Editado!')->html()->persistent();

        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);
        $usuario->delete();
        alert()->error('El usuario <span class="red-text bold">'.$usuario->name.'</span> fue eliminado', '¡Usuario Eliminado!')->html()->persistent("Ok");
        return redirect()->route('usuarios.index');
    }
}
