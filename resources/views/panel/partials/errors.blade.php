@if(count($errors) > 0)	
	<div class="hide">{!! $errMsg = '' !!}</div>
	@foreach($errors->all() as $error)
		<div class="hide">{{ $errMsg = $errMsg."<b><li>".$error."</li></b>" }}</div>
	@endforeach

	<script type="text/javascript">
		swal({
		  title: "Error!",
		  text: "<ul>{!! $errMsg !!}</ul>",  
		  type: "error",
		  html: true,
		  confirmButtonText: "Ok"
		});
	</script>
@endif