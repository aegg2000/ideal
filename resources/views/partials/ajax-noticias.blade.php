@foreach($noticias as $noticia)

	<section class="card">
		<div class="card-content">

			<h5 class="titulo truncate">{{ $noticia->titulo }}</h5>

			@if(!is_null($noticia->urlImg))
				<img class="responsive-img col s12 l4" src="{{ url('images/miniatura/'. $noticia->urlImg) }}" alt="{{ $noticia->titulo }}">
			@endif
			<p class="resumen col s12 l8">{{ $noticia->resumen }}</p>
			<div class="clear"></div>
		</div>
		<div class="card-action row unrow">
			<div class="col s7 fecha">{{ date_format($noticia->created_at, 'd-m-Y') }}</div>
			<div class="col s5">
				<a class="waves-effect waves-light btn right customBtn" href="{{ route('noticia', $noticia->slug) }}">Ver Noticia</a>
			</div>
		</div>
	</section>

@endforeach

<div id="loader" class="container center-align">
	@if($noticias->hasMorePages())
		<a id="vermas" class="vermas" href="{{ $noticias->nextPageUrl() }}">Cargar más noticias</a>
	@else
		<p class="blue-text">No hay más noticias</p>
	@endif
</div>
