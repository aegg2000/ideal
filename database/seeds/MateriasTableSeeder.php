<?php

use Illuminate\Database\Seeder;

class MateriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nivel_id = array("1","2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");
        $materia = array("Ingles", "Matematica", "Lenguas", "Historia", "Quimica", "Fisica");
        for($j=0; $j<=5; $j++){
        	for($i=0; $i<=11; $i++){
	    		DB::table('materias')->insert([
		            'nombre' => $materia[$j],
		            'nivel_id' => $nivel_id[$i],
	        	]);
    		}
        }        
    }
}
