<div  class="center centrado-div">
	<h3 class="blue-text">Bienvenido a la Zona Ideal</h3>	
	<h5>Aquí encontrarás material de estudio desde preescolar hasta bachillerato</h5>
	 <!-- Modal Trigger -->
	<a class="waves-effect waves-light btn modal-trigger blue foot" href="#modal1">
	Solicitud de documentos</a>

	<!-- Modal Structure -->
	<div id="modal1" class="modal modal-fixed-footer">
		<div class="modal-content row">
			<h4 class="blue-text col s12">Solicitud de Documentos</h4>
			{{-- FORMULARIO --}}
			{!! Form::open(['route' => 'documentos', 'method' => 'POST', 'class' => 'col s12 formDatos']) !!}
	   			<h5 class="italic-text sub-text">Datos del Representante</h5>
		        <div class="input-field col m6 s12">
		          <input id="representante_name" required name="representante_name" type="text" class="validate">
		          <label for="representante_name">Nombre y Apellido</label>
		        </div>
		        <div class="input-field col m6 s12">
		          <input id="representante_ci" required name="representante_ci" type="text" class="validate">
		          <label for="representante_ci">Cédula</label>
		        </div>
		        <div class="input-field col m6 s12">
		          <input id="representante_telf" required name="representante_telf" type="tel" class="validate" pattern="[0][4][2,1][4,6,2]\d{7}|[0][2][1,3,4,5,6,7,8,9][0-9]\d{7}">
		          <label for="representante_telf">Teléfono de Contacto</label>
		        </div>
		        <div class="input-field col m6 s12">
		          <input id="representante_email" required name="representante_email" type="email" class="validate">
		          <label for="representante_email">Correo Electrónico</label>
		        </div>

	   			<h5 class="italic-text sub-text col s12">Datos del Alumno</h5>
		        <div class="input-field col s12">
		          <input id="alumno_name" required name="alumno_name" type="text" class="validate">
		          <label for="alumno_name">Nombre y Apellido</label>
		        </div>
		        <div class="input-field col m6 s12">
		          <input id="alumno_ci" required name="alumno_ci" type="text" class="validate">
		          <label for="alumno_ci">Cédula</label>
		        </div>
		        <div class="input-field col m6 s12">
					{!! Form::select('grado', [
						'' 			=> 'Seleccione una opción',
						'Preescolar' 	=> ['Preescolar' => 'Preescolar'],
						'Primaria' 		=> [
							'Primer Grado' => '1er Grado',
							'Segundo Grado' => '2do Grado',
							'Tercer Grado' => '3er Grado',
							'Cuarto Grado' => '4to Grado',
							'Quinto Grado' => '5to Grado',
							'Sexto Grado' => '6to Grado',
						],
						'Bachillerato' 	=> [
							'Primer Año' => '1er Año',
							'Segundo Año' => '2do Año',
							'Tercer Año' => '3er Año',
							'Cuarto Año' => '4to Año',
							'Quinto Año' => '5to Año',
						],
					], ['required']) !!}
					{!! Form::label('grado', 'Nivel Escolar') !!}
				</div>
	   			<h5 class="italic-text sub-text col s12">Documentos a Solicitar</h5>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="cons_estudio" name="cons_estudio" />
			      <label for="cons_estudio">Constancia de Estudio</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="carta_aceptacion" name="carta_aceptacion" />
			      <label for="carta_aceptacion">Carta de Aceptación</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="cons_insc" name="cons_insc" />
			      <label for="cons_insc">Constancia de Inscripción</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="boleta_retiro" name="boleta_retiro"/>
			      <label for="boleta_retiro">Boleta de Retiro</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="cons_promo" name="cons_promo"/>
			      <label for="cons_promo">Constancia de Promoción</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="solvencia" name="solvencia"/>
			      <label for="solvencia">Solvencia</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="notas_certi" name="notas_certi"/>
			      <label for="notas_certi">Notas Certificadas</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="rif" name="rif" />
			      <label for="rif">RIF</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="mppe" name="mppe"/>
			      <label for="mppe">Inscripción en el MPPE</label>
			    </p>
	   			<p class="col m6 s12">
			      <input type="checkbox" id="registro_mer" name="registro_mer" />
			      <label for="registro_mer">Registro Mercantil</label>
			    </p>
		        <div class="input-field col s12">
		          <textarea id="motivo" name="motivo" class="materialize-textarea" required></textarea>
		          <label for="motivo">Motivo de Solicitud</label>
		        </div>
		        <div class="row">
					<div class="col s12">
						<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action" onclick="enviarDatos()">Enviar Datos</button>
					</div>
				</div>
		   	{{ Form::close() }}
		</div>
		<div class="modal-footer">
			<a href="#" id="modal_enviar" class="modal-action modal-close waves-effect waves-green btn-flat ">Salir</a>
		</div>
	</div>
</div>
