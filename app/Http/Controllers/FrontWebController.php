<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Noticia;
use App\Agenda;
use App\Biblioteca;
use App\Materia;
use App\Http\Requests\DatosRequest;
use Mail;
use App\Mail\SolicitudRespuesta;
use App\Mail\SolicitudDocumentos;
use Cache;

class FrontWebController extends Controller
{
    public function inicio(Request $request)
    {   
        $noticias = Noticia::buscar($request->titulo)->orderBy('created_at','DSC')->simplePaginate(7);

        if($request->ajax()){
            $noticias->withPath('?titulo='.$request->titulo);
            return view('partials.ajax-noticias')->with('noticias', $noticias);
        }

        date_default_timezone_set ( 'America/Caracas' );
        $fecha = date('Y-m-d');

        Agenda::where('fechaEvento','<', $fecha)->delete(); //Elimina todos los eventos anteriores a la fecha actual.
        
        if (Cache::has('agenda')) {
            $hoy = Cache::get('hoy');
            $json = Cache::get('agenda');
        }else{
            $agenda = Agenda::all();
            $hoy = $agenda->where('fechaEvento', $fecha); //Eventos del dia.
            $eventos = $agenda->groupBy('fechaEvento')->toArray(); //Data del Calendario.
            $json = array();
            foreach ($eventos as $k => $ev) {
                $contenido = array();
                $fecha = date_create($k);
                $fecha = date_format($fecha, 'm-d-Y');
                foreach ($ev as $key => $value) {
                    $horaInicio = '';
                    $horaFin = '';
                    $conte = '';
                    if(is_null($value['horaInicio'])){
                        $horaInicio = 'hide';
                    }
                    if(is_null($value['horaFin'])){
                        $horaFin = 'hide';
                    }
                    if(is_null($value['contenido'])){
                        $conte = 'hide';
                    }
                    $contenido[$key] = '<span><div class="card-panel"><p>' . $value['titulo'] . '</p><p class="' . $conte . ' resumen-evento black-text">' . $value['contenido'] . '</p><hr class="' . $horaInicio . '"><div class="' . $horaInicio . ' horas-evento row unrow blue-text darken-2"><span class="col s6 center">Inicio: ' . $value['horaInicio'] . '</span><span class="' . $horaFin .' col s6 center">Fin: ' . $value['horaFin'] . '</span></div></div></span>';
                }
                $json[$fecha] = $contenido;
            }
            $json = json_encode($json);
            Cache::put('agenda', $json, 720);
            Cache::put('hoy', $hoy, 720);
        }            

        return view('inicio')
            ->with('noticias', $noticias)
            ->with('json', $json)
            ->with('hoy', $hoy);
    }

    public function noticia($slug)
    {
        if(Cache::has($slug)){
            $noticia = Cache::get($slug);
            return view('noticia')->with('noticia', $noticia);    
        }
        $noticia = Noticia::where('slug', $slug)->first();
        Cache::put($slug, $noticia, 1440);
        return view('noticia')->with('noticia', $noticia);
    }

    public function solicitudDocumentos(Request $request){

        date_default_timezone_set ( 'America/Caracas' );
        $fecha = date('Ymd_hisa', time());

        $datos = $request->toArray();
        $pdf = \PDF::loadView('pdf.solicitud', $datos);
        $path = public_path().'/solicitudes/solicitud_'. $datos['representante_ci'] . '_' . $fecha .'.pdf';
        $pdf->save($path);

        Mail::to('ueideal@gmail.com')->queue(new SolicitudDocumentos($datos, $path));
        Mail::to($datos['representante_email'])->queue(new SolicitudRespuesta($path));

        unlink($path);

        alert()->success('Le hemos enviado un correo electronico con los pasos para retirar sus documentos.' ,'Datos Enviados Exitosamente')->html()->persistent();

        return redirect()->route('zonaIdeal');
    }

    public function zonaIdeal(Request $request)
    {
        $biblioteca = Biblioteca::buscar($request->buscar)->filtrar($request->nivel_id, $request->materia_id)->paginate(10);


        if($request->ajax()){
            $biblioteca->withPath('?buscar='.$request->buscar);
            return view('partials.articulosZona')->with('biblioteca', $biblioteca);
        }
        
        if(Cache::has('grado')){
            $grado = Cache::get('grado');
        }else{
            $materias = Materia::all();    
            $grado = array();
            for ($i=1; $i <= 11 ; $i++) { 
                $grado[$i] = $materias->where('nivel_id', $i+1);
            }
            Cache::put('grado', $grado, 20160);
        }

        $preescolar = Biblioteca::where('nivel_id', 1)->paginate(4);     

        return view('zonaideal')
            ->with('preescolar', $preescolar)
            ->with('grado', $grado);
    }
}
