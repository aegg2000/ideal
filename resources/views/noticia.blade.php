@extends('layouts.main')

@section('titulo')
	{{ $noticia->titulo }}
@endsection

@section('contenido')
<div class="contenedor">
	<div class="row unrow">
    	<div id="noticia-completa" class="card-panel col s12">
    		<h4 class="titulo-noticia-completa">{{ $noticia->titulo }}</h4>
    		<span class="grey-text lighten-1" style="font-style: italic;">Por <span class="black-text" style="font-weight: 500">{{ $noticia->user->name }}</span> el <span class="black-text" style="font-weight: 500">{{ date_format($noticia->created_at, 'd-m-Y') }}</span></span><br>
    		@if(!is_null($noticia->urlImg))
    		<div class="center">
    			<img class="responsive-img img-noticia" src="{{ url('images/normal/'. $noticia->urlImg) }}" alt="{{ $noticia->titulo }}">
    		</div>
    		@endif    		
    		{!! $noticia->contenido !!}
	   	</div>
    </div>
</div>   
@endsection

@section('script')
@endsection