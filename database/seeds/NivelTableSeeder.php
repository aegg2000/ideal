<?php

use Illuminate\Database\Seeder;

class NivelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $niveles = array("Preescolar","Primer Grado", "Segundo Grado", "Tercer Grado", "Cuarto Grado", "Quinto Grado", "Sexto Grado", "Primer Año", "Segundo Año", "Tercer Año", "Cuarto Año", "Quinto Año");
        for($i=0; $i<=11; $i++){
    		DB::table('nivels')->insert([
	            'nombre' => $niveles[$i],
        	]);
    	}
    }
}
