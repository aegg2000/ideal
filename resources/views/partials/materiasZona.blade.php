@foreach($materias as $materia)
	@if($materia->nivel_id > 1 and $materia->nivel_id < 8)
		<a href="{{ route('zonaIdeal', ['nivel_id' => $materia->nivel_id, 'materia_id' => $materia->id]) }}" class="collection-item item-primaria">{{ $materia->nombre }}</a>
	@elseif($materia->nivel_id > 7 and $materia->nivel_id < 13)
		<a href="{{ route('zonaIdeal', ['nivel_id' => $materia->nivel_id, 'materia_id' => $materia->id]) }}" class="collection-item item-bachillerato">{{ $materia->nombre }}</a>
	@endif
@endforeach