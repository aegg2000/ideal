@extends('panel.layouts.main')

@section('titulo')
	Actualizar Slider
@endsection

@section('contenido')
<div class="contenedor">
<div class="row card center-align">
	{!! Form::open(['route' => ['slider.update', $id], 'method' => 'PUT', 'files' => true, 'class' => 'col s12']) !!}

	<div class="row">
		<div class=" col s12 center input-field">
			<input name="urlImg" type="file" class="dropify" data-height="300" data-max-file-size="5M" data-allowed-file-extensions="png jpg jpeg" data-default-file="{{ asset($name) }}"/>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action">Actualizar Slider</button>
		</div>
	</div>
	{!! Form::close() !!}
</div>

<div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/slider') }}">
      <i class="large material-icons">arrow_back</i>
    </a>
</div>
</div>
@endsection

@section('script')
	<script type="text/javascript">
		$('.dropify').dropify({
            messages: {
                'default': 'Arrastre una imagen o haga click',
                'replace': 'Arrastre una imagen o haga click para reemplazar',
                'remove':  'Remover',
                'error':   'Ooops, algo salio mal.'
            }
        });
	</script>
@endsection