@extends('panel.layouts.main')

@section('titulo')
	Agenda
@endsection

@section('contenido')
<div class="contenedor">
<div class="card">
	@if(!is_null($eventos))
		<div class="card-content light-blue accent-4">
			<span class="card-title white-text center-align">Lista de Eventos</span>		
		</div>
		<table class="striped responsive-table">

		    <thead>
		      <tr>
				  <th></th>
		          <th>ID</th>
		          <th>Titulo</th>
		          <th>Fecha</th>
		          <th>Inicio</th>
		          <th>Fin</th>
		          <th>Publicado Por</th>
		          <th>Acción</th>
		      </tr>
		    </thead>
			
		    <tbody>
		    	@foreach($eventos as $evento)
		    		<tr>
		    			<td></td>
		    			<td>{{ $evento->id }}</td>
		    			<td>{{ $evento->titulo }}</td>
		    			<td>{{ $evento->fechaEvento }}</td>
		    			<td>{{ $evento->horaInicio }}</td>
		    			<td>{{ $evento->horaFin }}</td>
		    			<td>{{ $evento->user->name }}</td>
		    			<td>
			    			<a href="{{ route('agenda.edit', $evento->id) }}"><i class="material-icons">mode_edit</i></a>

			    			<a href="#!" onclick="confirmar({{ $evento->id }}, 'agenda')"><i class="material-icons red">delete_forever</i></a>
		    			</td>
		    		</tr>
		    	@endforeach
		    </tbody>
		  </table>
	      <div class="container center-align">
	      	{!! $eventos->links() !!}
	      </div>
	      <div class="divider"></div>
	 </div>
	@endif
 <br>
 <div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/agenda/crear') }}">
      <i class="large material-icons">add</i>
    </a>
 </div>
</div>
@endsection

@section('script')
	@include('panel.partials.sweetSure')
@endsection