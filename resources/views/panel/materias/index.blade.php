@extends('panel.layouts.main')

@section('titulo')
	Materias
@endsection

@section('contenido')
<div class="contenedor">
<div class="card">
	<div class="card-content light-blue accent-4">
		<span class="card-title white-text center-align">Lista de Materias</span>		
	</div>
	<table class="striped responsive-table">

	    <thead>
	      <tr>
			  <th></th>
	          <th>ID</th>
	          <th>Materia</th>
	          <th>Nivel Escolar</th>
	          <th>Acción</th>          
	      </tr>
	    </thead>
	    <tbody>
	    	@foreach($materias as $materia)
	    		<tr>
	    			<td></td>
	    			<td>{{ $materia->id }}</td>
	    			<td>{{ $materia->nombre }}</td>
	    			<td>{{ $materia->nivel->nombre }}</td>
	    			<td>
		    			<a href="{{ route('materias.edit', $materia->id) }}"><i class="material-icons">mode_edit</i></a>

		    			<a href="#!" onclick="confirmar({{ $materia->id }}, 'materias')"><i class="material-icons red">delete_forever</i></a>
	    			</td>
	    		</tr>
	    	@endforeach
	    </tbody>
	  </table>
      <div class="container center-align">
      	{!! $materias->links() !!}
      </div>
      <div class="divider"></div>
 </div>
 <br>
 <div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/materias/crear') }}">
      <i class="large material-icons">add</i>
    </a>
    <ul>
      <li><a href="{{ url('panel/biblioteca') }}" class="btn-floating green"><i class="material-icons">swap_horiz</i></a></li>
    </ul>
 </div>
</div>
@endsection