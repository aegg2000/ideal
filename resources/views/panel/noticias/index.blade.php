@extends('panel.layouts.main')

@section('titulo')
	Noticias
@endsection

@section('contenido')
<div class="contenedor">
<div class="card">
	<div class="card-content light-blue accent-4">
		<span class="card-title white-text center-align">Lista de Noticias</span>		
	</div>
	<table class="striped responsive-table">

	    <thead>
	      <tr>
			  <th></th>
	          <th>ID</th>
	          <th>Titulo</th>
	          <th>Publicado Por</th>
	          <th>Acción</th>          
	      </tr>
	    </thead>

	    <tbody>
	    	@foreach($noticias as $noticia)
	    		<tr>
	    			<td></td>
	    			<td>{{ $noticia->id }}</td>
	    			<td>{{ $noticia->titulo }}</td>
	    			<td>{{ $noticia->user->name }}</td>
	    			<td>
		    			<a href="{{ route('noticias.edit', $noticia->id) }}"><i class="material-icons">mode_edit</i></a>

		    			<a href="#!" onclick="confirmar({{ $noticia->id }}, 'noticias')"><i class="material-icons red">delete_forever</i></a>
	    			</td>
	    		</tr>
	    	@endforeach
	    </tbody>
	  </table>
      <div class="container center-align">
      	{!! $noticias->links() !!}
      </div>
      <div class="divider"></div>
 </div>
 <br>
 <div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/noticias/crear') }}">
      <i class="large material-icons">add</i>
    </a>
 </div>
</div>
@endsection

@section('script')
	@include('panel.partials.sweetSure')
@endsection