<script type="text/javascript">
    function confirmar(id, pagina){
        swal({
          title: "¿Estas Seguro?",
          text: "Estas por borrar un usuario",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Borrar",
          cancelButtonText: "Cancelar",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href = pagina+"/"+id+"/eliminar";
            }
        });
    }
</script>