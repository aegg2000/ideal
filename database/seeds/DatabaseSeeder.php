<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(NivelTableSeeder::class);
        //factory(App\Noticia::class, 50)->create();
        $this->call(MateriasTableSeeder::class);
        //factory(App\Biblioteca::class, 18)->create();
        //factory(App\Agenda::class, 15)->create();
    }
}
