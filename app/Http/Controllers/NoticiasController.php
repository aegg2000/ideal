<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Noticia;
use Image;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::orderBy('id', 'DSC')->paginate(20);
        return view('panel.noticias.index')->with('noticias', $noticias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.noticias.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $noticia = new Noticia($request->all());
        $noticia->user_id = Auth::User()->id;

        if($request->file('urlImg')){
            $file = $request->file('urlImg');
            $name = Auth::user()->id . '_' . time() . '.' . $file->getClientOriginalExtension();
            $url = '/img/noticias/'. $name;
            $path = public_path($url);
            $image = Image::make($file)->save($path);
            $noticia->urlImg = $name;
        }

        $noticia->save();

        alert()->success('<span class="green-text bold">'.$noticia->titulo.'</span> se ha creado' ,'Noticia Creada Exitosamente')->html()->persistent();

        return redirect()->route('noticias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $noticia = Noticia::find($id);
        return view('panel.noticias.editar')->with('noticia', $noticia);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticia = noticia::find($id);
        $oldname = $noticia->urlImg;
        $noticia->fill($request->all());

        if($request->file('urlImg')){
            unlink(public_path('img/noticias/' . $oldname));
            $file = $request->file('urlImg');
            $name = Auth::user()->id . '_' . time() . '.' . $file->getClientOriginalExtension();
            $url = '/img/noticias/'. $name;
            $path = public_path($url);
            $image = Image::make($file)->save($path);
            $noticia->urlImg = $name;
        }       
        
        $noticia->save();

        alert()->success('Noticia: <span class="green-text bold">'.$noticia->titulo.'</span> fue editado' ,'¡Noticia Editada!')->html()->persistent();

        return redirect()->route('noticias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $noticia = Noticia::find($id);
        if(!is_null($noticia->urlImg)){
            unlink(public_path('img/noticias' . $noticia->urlImg));    
        }        

        alert()->error('Noticia: <span class="red-text bold">' . $noticia->titulo . '</span> fue eliminada', '¡Noticia Eliminada!')->html()->persistent("Ok");

        $noticia->delete();
        return redirect()->route('noticias.index');
    }
}
