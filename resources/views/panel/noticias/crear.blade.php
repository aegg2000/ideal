@extends('panel.layouts.main')

@section('titulo')
	Crear Noticia
@endsection

@section('contenido')
<div class="contenedor">
	<div class="row card center-align">
		{!! Form::open(['route' => 'noticias.store', 'method' => 'POST', 'files' => true,'class' => 'col s12']) !!}
			<div class="row">
				<article id="datosNoticia" class="col s12">
					<div class="input-field col s12">
						{!! Form::text('titulo', null, ['class' => 'validate input-border-color titulo-noticia', 'placeholder' => 'Titulo de la Noticia','required']) !!}
					</div>

					<div id="resumen_noticia" class=" input-field col s12">
						<textarea id="resumen" name="resumen" class="validate materialize-textarea" data-length="155"></textarea>
						{!! Form::label('resumen', 'Resumen') !!}
					</div>
					<div class="col s12 input-field"></div>
					<div class="col s12 input-field"></div>
					<div id="texto-noticia" class=" col s8">
						<textarea id="contenido" name="contenido"></textarea>
					</div>

					<div id="imagen-carga" class=" col s4 center">
						<input name="urlImg" type="file" class="dropify" data-height="300" data-max-file-size="3M" data-allowed-file-extensions="png jpg jpeg" data-min-width="640" data-min-height="360"/>
					</div>

				</article>
			</div>
			<div class="row">
				<div class="col s12">
					<button id="btnForm" class="btn waves-effect waves-light" type="submit" name="action">Publicar Noticia</button>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
	<div class="fixed-action-btn">
	    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/noticias') }}">
	      <i class="large material-icons">arrow_back</i>
	    </a>
	</div>
</div>

@endsection

@section('script')
	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script>
	<script type="text/javascript">
		$('.dropify').dropify({
            messages: {
                'default': 'Arrastre una imagen o haga click',
                'replace': 'Arrastre una imagen o haga click para reemplazar',
                'remove':  'Remover',
                'error':   'Ooops, algo salio mal.'
            }
        });

        CKEDITOR.replace( 'contenido' );

        $(document).ready(function() {
    		$('textarea#resumen').characterCounter();
  		});

	</script>
@endsection
