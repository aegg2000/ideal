@extends('layouts.main')

@section('activeContacto')
	activePage
@endsection

@section('contenido')
<div class="contenedor">
	<div class="row">
		<div class="col s12">
			<article id="informacion" class="card-panel">
				<div class="row"><span class="panel1 col m4 s12">Dirección</span><span class="panel2 col m8 s12">Calle La Peña, Lechería, Anzoátegui</span></div>
				<div class="row"><span class="panel1 col m4 s12">Telefono</span> <span class="panel2 col m8 s12">0281 286 10 80</span></div>
				<div class="row"><span class="panel1 col m4 s12">Email</span> <span class="panel2 col m8 s12">infoueideal@gmail.com</span></div>
			</article>
		</div>
	</div>
	<div class="row">
		<div id="mapa" class="col m8 s12">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4670.105911974516!2d-64.68956451277924!3d10.174533207433894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd925705bff01f97!2sIDEAL!5e0!3m2!1ses-419!2sve!4v1498337693029" width="100%" height="460" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div id="formulario" class="col m4 s12 card-panel" style="margin-top: 0;">
			<h4 class="center-align">Escríbenos</h4>
			<form action="">
				<div class="row unrow">
					<div class="input-field col s12">
						<select name="motivo">
							<option value="" disabled selected>Seleccione una opción</option>
							<option value="cupos">Consulta de Cupos</option>
							<option value="notas">Consulta de Notas</option>
							<option value="sugerencias">Sugerencias</option>
							<option value="otros">Otros</option>
						</select>
						<label>Motivo</label>
					</div>
				</div>

				<div class="row unrow">
					<div class="input-field col s12">
						<input type="text" name="nombre" class="validate">
						<label for="nombre">Nombre y Apellido</label>
					</div>
				</div>

				<div class="row unrow">
					<div class="input-field col s12">
						<input type="email" name="email" class="validate">
						<label for="email">Correo Electronico</label>
					</div>
				</div>

				<div class="row unrow">
					<div class="input-field col s12">
						<textarea id="consulta" class="materialize-textarea"></textarea>
						<label for="consulta">Consulta</label>
					</div>
				</div>

				<div class="row">
					<button disabled="disabled" class="btn waves-effect waves-light customBtn col s12" type="submit" name="enviar">Enviar
					    <i class="material-icons right">send</i>
				  	</button>
			  	</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('select').material_select();
	  	});
	</script>
@endsection
