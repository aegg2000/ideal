<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Biblioteca;
use App\Materia;

class BibliotecaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materiales = Biblioteca::orderBy('id', 'ASC')->paginate(20);
        return view('panel.biblioteca.index')->with('materiales', $materiales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.biblioteca.crear');
    }

    public function selectAjax(Request $request)
    {
        if($request->ajax()){
            $materias = Materia::where('nivel_id',$request->nivel_id)->pluck("nombre","id")->all();
            $data = view('panel.biblioteca.ajax-select',compact('materias'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $material = new Biblioteca($request->all());
        $material->user_id = Auth::User()->id;
        $material->save();

        alert()->success('El material <span class="green-text bold">'.$material->titulo.'</span> fue agregado', '¡Material Agregado!')->html()->persistent();

        return redirect()->route('biblioteca.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Biblioteca::find($id);
        return view('panel.biblioteca.editar')->with('material', $material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $material = Biblioteca::find($id);
        $material->fill($request->all());
        $material->save();

        alert()->success('El material <span class="green-text bold">'.$material->titulo.'</span> fue agregado', '¡Material Actualizado!')->html()->persistent();

        return redirect()->route('biblioteca.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Biblioteca::find($id);

        alert()->error('El material <span class="red-text bold">' . $material->titulo . '</span> fue eliminado', '¡Material Eliminado!')->html()->persistent("Ok");

        $material->delete();
        return redirect()->route('biblioteca.index');
    }
}
