<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $table = 'materias';

    protected $fillable = ['nombre', 'nivel_id'];

    public function bibliotecas(){
        return $this->hasMany('App\Biblioteca');
    }

    public function nivel(){
    	return $this->belongsTo('App\Nivel');
    }
}
