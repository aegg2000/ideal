@extends('panel.layouts.home')

@section('contenido')
	<article id="homePanel" class="inicio-panel row unrow">
		<section class="col s12 center">
			<img class="responsive-img" src="{{ url('images/logologin/logo.png') }}">
		</section>
		<section class="col s12 center white-text fontcustom">
			<h3>Bienvenido al Panel de Adminisitración</h3>
			<h3 id="nombreusuario"><span class="amarillo-ideal">{{ Auth::user()->name }}</span></h3>
			<h5>Es momento de gestionar los datos de la pagina web.</h5>
			<h6>Seleccione una opción:</h6>
		</section>
		{!! 
			$disabledUsuario = $disabledAgenda = $disabledBiblioteca = $disabledNoticia = '';
			$colorUsuario = 'purple darken-1';
			$colorAgenda = 'light-green darken-1';
			$colorNoticia = 'blue darken-1';
			$colorBiblioteca = 'red darken-2'
		!!}
		@if(is_null(Auth::user()->usuario))
			<div class="hide">{!! $disabledUsuario = 'disabled'; $colorUsuario = '' !!}</div>
		@endif
		@if(is_null(Auth::user()->noticia))
			<div class="hide">{!! $disabledNoticia = 'disabled'; $colorNoticia = '' !!}</div>
		@endif
		@if(is_null(Auth::user()->biblioteca))
			<div class="hide">{!! $disabledBiblioteca = 'disabled'; $colorBiblioteca = '' !!}</div>
		@endif
		@if(is_null(Auth::user()->agenda))
			<div class="hide">{!! $disabledAgenda = 'disabled'; $colorAgenda = '' !!}</div>
		@endif
		<section class="col s12 center">
			<a class="{!! $disabledUsuario !!}" href="{{ url('/panel/usuarios') }}">
				<div class="card-panel hoverable {!! $colorUsuario !!} white-text col s2custom">
					<p><i class="fa fa-users fa-custom"></i></p>
					<p>Usuarios</p>
				</div>
			</a>
			<a class="{!! $disabledNoticia !!}" href="{{ url('/panel/noticias') }}">
				<div class="card-panel hoverable {!! $colorNoticia !!} white-text col s2custom">
					<p><i class="fa fa-newspaper-o fa-custom"></i></p>
					<p>Noticias</p>
				</div>
			</a>
			<a class="{!! $disabledAgenda !!}" href="{{ url('/panel/agenda') }}">
				<div class="card-panel hoverable {!! $colorAgenda !!} white-text col s2custom">
					<p><i class="fa fa-calendar fa-custom"></i></p>
					<p>Agenda</p>
				</div>
			</a>
			<a class="{!! $disabledBiblioteca !!}" href="{{ url('/panel/biblioteca') }}">
				<div class="card-panel hoverable {!! $colorBiblioteca !!} white-text col s2custom">
					<p><i class="fa fa-book fa-custom"></i></p>
					<p>Biblioteca</p>
				</div>
			</a>
			<a href="{{ url('/panel/docs') }}">
				<div class="card-panel hoverable teal darken-2 white-text col s2custom">
					<p><i class="fa fa-file-text fa-custom"></i></p>
					<p>Documentacion</p>
				</div>
			</a>
			<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
				<div class="card-panel hoverable brown darken-1 white-text col s2custom">
					<p><i class="material-icons fa-custom">power_settings_new</i></p>
					<p>Cerrar Sesión</p>
				</div>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
		</section>
	</article>
@endsection
