<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitudRespuesta extends Mailable
{
    use Queueable, SerializesModels;

    private $path;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($path)
    {
        $this->subject = "Solicitud de Documentos Realizada Exitosamente.";
        $this->path = $path;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.solicitudRespuesta')->attach($this->path, [
                'as' => 'solicitud.pdf',
                'mime' => 'application/pdf',
            ]);
    }
}
