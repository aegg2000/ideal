<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Solicitud de Documentos</title>

	<style type="text/css">
		.clear{
			clear: both;
		}
		img{
			float: left;width: 120px; height: 70px;
		}
		header p{
			float: left;
			margin-left: 70px;
			position: absolute; top: 40px;
			text-decoration: underline;
			font-weight: bolder;
		}
		section{
			
		}
		span{
			font-weight: bold;
		}
		#solicitud{
			text-align: center;
		}
		#datos{
			text-align: justify-all;
		}
		#documentos span{
			text-decoration: underline;
		}
		#documentos{ position: absolute; top: 80px; }
		.col{
			float: left;
			width: 50%;
			margin-left: 30px;
		}
		.selected{
			font-size: 16px;
		}
		.custom-under{
			position: relative;
		}
		.custom-under::after{
			content: '';
			background-color: black;
			height: 1px; 
			width: 92%;
			position: absolute;
			top: 18px;
			left: 0px;		
		}
		#fecha, #firma{
			text-align: center;
			width: 25%;
		}
		#fecha{
			float: left;
		}
		#firma{
			float: right;
		}
		.custom-under-2{
			position: relative;
		}
		.custom-under-2::after{
			content: '';
			background-color: black;
			height: 1px; 
			width: 100%;
			position: absolute;
			top: 18px;
			left: 0px;		
		}
		.custom-under-3{
			position: relative;
		}
		.custom-under-3::after{
			content: '';
			background-color: black;
			height: 1px; 
			width: 100%;
			position: absolute;
			top: 18px;	
		}
		#entregado{
			float: left;
			width: 30%;
		}
		#recibido{
			float: right;
			margin-right: 85px;
			width: 30%;
		}
	</style>
</head>
<body>
	<header>

		<img src="img/logo.png">
		<p>INSTITUTO DE ENSEÑANZA AMÉRICA LATINA</p>

	</header>

	<section class="clear">
		<p id="solicitud">SOLICITUD</p>
		<p id="datos">
			Yo, <span>{{ $representante_name }}</span>, titular de la C.I. Nº: <span>{{ $representante_ci }}</span>
			Representante legal del alumno <span>{{ $alumno_name }}</span>, titular de la 
			C.I. Nº: <span>{{ $alumno_ci }}</span> alumno del grado/año <span>{{ $grado }}</span> solicito:
		</p>

		<div id="documentos">
			<div class="col">
				@if(isset($cons_estudio))
					<p><span class="selected">X</span> CONSTANCIA DE ESTUDIO</p>
				@else
					<p><span class="selected">_</span> CONSTANCIA DE ESTUDIO</p>
				@endif

				@if(isset($cons_insc))
					<p><span class="selected">X</span> CONSTANCIA DE INSCRIPCION</p>
				@else
					<p><span class="selected">_</span> CONSTANCIA DE INSCRIPCION</p>
				@endif

				@if(isset($cons_promo))
					<p><span class="selected">X</span> CONSTANCIA DE PROMOCION</p>
				@else
					<p><span class="selected">_</span> CONSTANCIA DE PROMOCION</p>
				@endif

				@if(isset($notas_certi))
					<p><span class="selected">X</span> NOTAS CERTIFICADAS</p>
				@else
					<p><span class="selected">_</span> NOTAS CERTIFICADAS</p>
				@endif

				@if(isset($mppe))
					<p><span class="selected">X</span> INSCRIPCION EN EL MPPE</p>
				@else
					<p><span class="selected">_</span> INSCRIPCION EN EL MPPE</p>
				@endif
			</div>
			<div class="col">
				@if(isset($carta_aceptacion))
					<p><span class="selected">X</span> CARTA DE ACEPTACION</p>
				@else
					<p><span class="selected">_</span> CARTA DE ACEPTACION</p>
				@endif

				@if(isset($boleta_retiro))
					<p><span class="selected">X</span> BOLETA DE RETIRO</p>
				@else
					<p><span class="selected">_</span> BOLETA DE RETIRO</p>
				@endif

				@if(isset($solvencia))
					<p><span class="selected">X</span> SOLVENCIA</p>
				@else
					<p><span class="selected">_</span> SOLVENCIA</p>
				@endif

				@if(isset($rif))
					<p><span class="selected">X</span> RIF</p>
				@else
					<p><span class="selected">_</span> RIF</p>
				@endif

				@if(isset($registro_mer))
					<p><span class="selected">X</span> REGISTRO MERCANTIL</p>
				@else
					<p><span class="selected">_</span> REGISTRO MERCANTIL</p>
				@endif
			</div>
		</div>
		<div class="clear"></div>
		<p>Motivo: <span class="custom-under">{{ $motivo }}</span></p>

		<div style="display: none">{!! date_default_timezone_set ( 'America/Caracas' ) !!}</div>
		
		<div id="fecha">
			<p class="custom-under-2"><span>{{ date('d-m-Y')  }}</span><br>Fecha</p>
		</div>

		<div id="firma">
			<p class="custom-under-2"><br>Firma del Representante</p>
		</div>

		<div class="clear"></div>
		<div id="entregado">
			<p class="custom-under-3">Entregado por:</p>
			<p class="custom-under-3">Fecha entrega:</p>
		</div>
		
		<div id="recibido">
			<p class="custom-under-3">Recibido por:</p>	
		</div>
		
		

	</section>

</body>
</html>