@extends('layouts.main')

@section('titulo')
	¿Quiénes Somos?
@endsection

@section('activeNosotros')
	activePage
@endsection

@section('contenido')
	<br>
	<div class="row">
	<article class="nosotros col m10 s12">
	<div class="parallax-container">
      <div class="parallax"><img src="img/slider/slider_1.jpg"></div>
    </div>


    	<section id="historia" class="justify card-panel historia section scrollspy">
    	<h3>Nuestra Historia</h3>
	   	<p>El Instituto de Enseñanza América Latina (IDEAL) es el sueño hecho realidad de la Lcda. Aida Susana López Mercado, nuestra fundadora. Siempre creyó en la educación como el camino para que las personas alcanzaran sus sueños. La escuela era ese espacio, ese primer paso, para que las personas, en este caso los niños, no solo aprendieran los contenidos de un programa de estudios, o conocieran el mundo, sino que descubrieran sus verdaderos talentos y desarrollaran su creatividad.</p>

 	 	<p>Con esta idea presente, recibe a los primeros 26 alumnos de pre-escolar. Así inicia actividades nuestro Colegio “Walt Disney” en la quinta Olga Mary, ubicado en la Av. Lic. Diego Bautista Urbaneja en Lechería en el año 1967. Walt Disney, recientemente fallecido para la fecha, representaba el gran interés por el mundo de los niños y su imaginación, también es la imagen de la persona creativa, visionaria y emprendedora.</p>

      	<p>En 1972 nos mudamos a la sede de la Urb. El Peñonal, incorporando todos los grados de la Educación Primaria. Ante la receptividad de la comunidad iniciamos las actividades del Primer Año del Ciclo Básico Común en 1984. Y en 1985 cambiamos nuestro nombre por el que lleva actualmente y comienzan los trabajos de ampliación de nuestra planta física.</p>

      	<p>En la actualidad, en el IDEAL continuamos este sueño y hemos incorporado nuevos modelos, programas y estrategias, al mismo tiempo preservando nuestra tradición formadora a los servicios estudiantiles y educativos, permitiéndoles a nuestros alumnos desarrollar una perspectiva educativa propia y al tiempo que sea significativa para ellos.</p>
    	</section>


    <div class="parallax-container">
      <div class="parallax"><img src="img/slider/slider_2.jpg"></div>
    </div>


    	<section id="mision" class="justify card-panel row mivi section scrollspy">
		<div class="col m6 s12">
			<h3 class="center-align">Misión</h3>
    		<p class="center-align" style="font-size: 30px; font-style: italic;">Aprender con alegría.</p>
		</div>
		<div class="col m6 s12">
			<h3 class="center-align">Visión</h3>
    		<p>Somos una organización de alto desempeño que aplica un modelo educativo para formar jóvenes con conocimientos y habilidades de utilidad global.</p>
		</div>
    	</section>


    	<section id="valores" class="justify card-panel valores section scrollspy">
	    	<h3>Valores</h3>
		   	<p><span>Aprendizaje/Superación:</span> Es el sentido de desarrollo y transformación de nuestra organización y de cada uno de nuestros colaboradores en los diferentes aspectos y momentos del trabajo y actividad diaria. Es el propósito y resultado final de nuestras acciones. Se manifiesta como la oportunidad pata retar nuestras capacidades y habilidades y romper con las rutinas y resulta en la adquisición de conocimientos, conductas, habilidades, valores, formas de pensamiento útiles para mejorar el desempeño (logro de resultados) y alcanzar nuevas responsabilidades en nuestra organización. Se resume en dos preguntas ¿Qué aprendí? Y ¿A dónde llegue?</p>

		   	<p><span>Visión/Meta. Compromiso (Objetivo):</span> Todas nuestras acciones son impulsadas por intenciones profesionales y organizacionales y están dirigidas algún fin, sacar adelante lo que se nos ha encomendado, confiado y nuestra conciencia ha aceptado y nos hemos propuesto, sin darse por vencido, a hacer más de lo esperado en el riesgo de lo que no hay ¿Será posible? Es la pregunta que dispara nuestras acciones hacia adelante y terminan respondiendo ¿Lo logré?</p>

		   	<p><span>Trabajo en equipo (Quienes):</span> Somos los mejores en lo que hacemos. Solo en la participación conjunta se alcanzan los objetivos propuestos. Todos pertenecemos al equipo y aceptamos las responsabilidades del rol que nos corresponde y desempeñamos en la mejor de nuestras habilidades. Cuento con el trabajo de los otros miembros de la organización, igual que ellos cuentan con el mío. Todos estamos para servir al otro desde nuestras responsabilidades ¿Qué es lo que otros necesitan de mí? ¿Con que cuentan los demás de mí?</p>

		   	<p><span>Alegría (Como):</span> Reconocemos que el estado emocional es el principal ingrediente de nuestra organización. La certeza de saber que nuestras acciones tienen sentido y dirección nos brinda la oportunidad para reconocer el estado emocional necesario para seguir adelante, al mismo tiempo de disfrutar la actividad diaria y celebrar las metas alcanzadas. ¿Qué emoción describe lo que hago? ¿Qué me hace sentir el resultado obtenido?</p>
    	</section>


    <div class="parallax-container">
      <div class="parallax"><img src="img/slider/slider_3.jpg"></div>
    </div>


    	<section id="principios1" class="justify card-panel principios section scrollspy">
	    	<h4>Principios Organizacionales</h4>
	    	<ul class="browser-default">
	    		<li>La comunicación es asertiva entre todos los miembros, con el fin de lograr los objetivos de la organización.</li>
	    		<li>El éxito del IDEAL se fundamenta en el desarrollo del talento humano y la excelencia profesional.</li>
	    		<li>Atentos a los cambios sociales, así como a las actualizaciones académicas y tecnológicas para brindar un servicio de calidad.</li>
	    		<li>La innovación e investigación son fundamentales para alcanzar el éxito.</li>
	    	</ul>
    	</section>

    	<section id="principios2" class="justify card-panel principios section scrollspy">
	    	<h4>Principios Educacionales</h4>
	    	<ul class="browser-default">
	    		<li><span>La verdadera intención del educador es que el alumno aprenda.</span></li>
	    		<p>La verdadera intención del educador es que alumno aprenda: a conocerse a sí mismo y lo que requiere para conocer al mundo que lo rodea a través de los contenidos programados y la actividad programada. Lo importante es que el alumno aprenda; los contenidos y las actividades son solo medios para lograrlo y por lo tanto son prescindibles y se cambian cuando el alumno y/o la situación lo ameritan. <span>¿Qué puedo hacer para que mi alumno aprenda?</span></p>
	    		<li><span>El aprendizaje siempre está relacionado con emociones.</span></li>
	    		<p>Se disfruta el aprendizaje y se aprende disfrutando. Los dos aspectos van juntos y son indisolubles. Las emociones son el elemento fundamental para el aprendizaje. El aprendizaje es más efectivo y eficiente cuando está relacionado con un estado emocional positivo. El estado emocional positivo le da sentido personal al aprendizaje. El educador logra que el alumno aprenda cuando lo conoce emocionalmente. <span>¿Qué estado emocional necesita mi alumno para aprender?</span></p>
	    		<li><span>El educador influye en la decisión de aprender del alumno.</span></li>
	    		<p>Solo existe aprendizaje cuando el alumno desea y acepta la clase que le brinda el educador. No existe clase que se pueda dar si el alumno no desea recibirla. Para que el aprendizaje se dé solo hace falta el alumno y que este desee aprender. EL maestro para enseñar necesita al alumno para que este aprenda. Al alumno no le hace falta el maestro para aprender. El maestro se hará de la manera para que alumno acepte de agrado la clase que se le ofrece. <span>¿Qué hago para que mi alumno quiera aprender?</span></p>
	    		<li><span>El aprendizaje es una experiencia individual que se hace compañía de otros.</span></li>
	    		<p>El aprendizaje es una experiencia individual que se hace compañía de otros. Esto se logra por medio del método Socrático o el arte de la Pregunta. La pregunta que despierta la curiosidad, inicia la búsqueda y dispara una respuesta. La pregunta clave: <span>¿Qué aprendí y que aprendió mi alumno?</span></p>
	    		<li><span>El educador es responsable de los aprendizajes del alumno.</span></li>
	    		<p>Somos el último responsable sobre el resultado del aprendizaje del alumno. Es el educador el que posee los recursos necesarios para impartir el contenido, crear las condiciones para estimular el aprendizaje en el alumno y establecer niveles de aprendizajes exigentes. <span>¿Qué hice para que mi alumno aprenda?</span></p>
	    		<li><span>Todo alumno desea aprender.</span></li>
	    		<p>Lo cierto es que el alumno viene a la escuela con el enorme deseo de aprender del mundo que lo rodea. Él está ávido de embarcarse en la aventura de descubrir ese mundo y sentir como lo afecta (aprendizaje). Él está esperando que su maestro se lo enseñe.</p>
	    	</ul>
    	</section>
	</article>
	<article class="col m2 hide-on-small-only" style="position: fixed; left: 83%">
		<ul class="section table-of-contents">
			<li><a href="#historia">Nuestra Historia</a></li>
			<li><a href="#mision">Misión y Visión</a></li>
			<li><a href="#valores">Valores</a></li>
			<li><a href="#principios1">Principios Organizacionales</a></li>
			<li><a href="#principios2">Principios Educacionales</a></li>
		</ul>
	</article>
	</div>
	<div id="myBtn" style="display: none;" class="fixed-action-btn back-to-top">
    	<a href="#"  class="btn-floating btn waves-effect waves-light blue "><i class="material-icons">arrow_upward</i></a>
    </div>
@endsection

@section('script')
	<script type="text/javascript">

	$(document).ready(function(){
      $('.parallax').parallax();
    });

    $(document).ready(function(){
    	$('.scrollspy').scrollSpy();
  	});

  	jQuery(document).ready(function() {

		var offset = 50;
		var duration = 300;
		var duration2 = 150;

		jQuery(window).scroll(function() {

			if (jQuery(this).scrollTop() > offset) {

				jQuery('.back-to-top').animate({height: 'show'});

			} else {

				jQuery('.back-to-top').fadeOut(duration2);

			}

		});

		jQuery('.back-to-top').click(function(event) {

			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;

		})

	});

	</script>
@endsection
