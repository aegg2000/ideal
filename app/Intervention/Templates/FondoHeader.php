<?php

namespace App\Intervention\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class FondoHeader implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(340, 115);
    }
}