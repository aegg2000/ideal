<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DatosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'representante_name' => array('required','min:3','max:50','regex:/\b[a-zA-Z\s]{1,}$/'),
            'representante_ci' => 'required|numeric',
            'representante_telf' => array('required','numeric','regex:/[0][4][2,1][4,6,2]\d{7}|[0][2][1,3,4,5,6,7,8,9][0-9]\d{7}/'),
            'representante_email' => 'required|email',

            'alumno_name' => array('required','min:3','max:50','regex:/\b[a-zA-Z\s]{1,}$/'),
            'alumno_ci' => 'required|numeric',

            'grado' => 'required',
            'motivo' => 'required|min:10|max:95',

            'cons_estudio' => 'required_without_all:carta_aceptacion,cons_insc,boleta_retiro,cons_promo,solvencia,notas_certi,rif,mppe,registro_mer'
        ];
    }

    public function messages()
    {
        return [
            'representante_name.required' => 'Nombre del Representante es requirido.',
            'representante_name.regex' => 'Nombre del Representante no admite números.',
            'representante_name.min' => 'Nombre del Representante al menos 3 caracteres.',
            'representante_name.max' => 'Nombre del Representante máximo 50 caracteres.',
            'representante_ci.numeric' => 'Cédula del Representante debe ser numerico',
            'representante_telf.regex' => 'Número de Teléfono no cumple el patrón.',

            'alumno_name.required' => 'Nombre del Alumno es requirido.',
            'alumno_name.regex' => 'Nombre del Alumno no admite números.',
            'alumno_name.min' => 'Nombre del Alumno al menos 3 caracteres.',
            'alumno_name.max' => 'Nombre del Alumno máximo 50 caracteres.',
            'alumno_ci.numeric' => 'Cédula del Alumno debe ser numerico',
            
            'cons_estudio.required_without_all' => 'Debe seleccionar al menos un documento.'
        ];
    }
}
