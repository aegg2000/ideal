<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Noticia extends Model
{
	use Sluggable;

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }


    protected $table = 'noticias';

    protected $fillable = ['titulo', 'resumen', 'contenido', 'urlImg', 'user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function scopeBuscar($query, $titulo){
        return $query->where('titulo', 'LIKE', "%$titulo%");
    }
}