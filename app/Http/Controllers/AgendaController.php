<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;
use Cache;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventos = Agenda::orderBy('fechaEvento', 'ASC')->paginate(20);
        return view('panel.agenda.index')->with('eventos', $eventos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.agenda.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $evento = new Agenda($request->all());
        $evento->user_id = \Auth::User()->id;
        $evento->save();

        // Refrescar cache
        Cache::forget('agenda');

        alert()->success('Evento <span class="green-text bold">'.$evento->titulo.'</span> fue agregado', '¡Evento Agregado!')->html()->persistent();

        return redirect()->route('agenda.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evento = Agenda::find($id);
        return view('panel.agenda.editar')->with('evento', $evento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $evento = Agenda::find($id);
        $evento->fill($request->all());
        $evento->save();

        Cache::forget('agenda');

        alert()->success('Evento <span class="green-text bold">'.$evento->titulo.'</span> fue modificado', '¡Evento Actualizado!')->html()->persistent();

        return redirect()->route('agenda.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $evento = Agenda::find($id);
        $evento->delete();

        Cache::forget('agenda');
        
        alert()->error('Evento <span class="red-text bold">'.$evento->titulo.'</span> fue eliminado', '¡Evento Eliminado!')->html()->persistent();
        return redirect()->route('agenda.index');
    }
}
