<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitudDocumentos extends Mailable
{
    use Queueable, SerializesModels;

    public $datos;
    private $path;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($datos, $path)
    {
        $this->subject = "Nueva solicitud de documentos.";
        $this->datos = $datos;
        $this->path = $path;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.solicitudDocumentos')->attach($this->path, [
                'as' => 'solicitud.pdf',
                'mime' => 'application/pdf',
            ]);;
    }
}
