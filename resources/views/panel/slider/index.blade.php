@extends('panel.layouts.main')

@section('titulo')
	Panel Slider
@endsection

@section('contenido')
<div class="contenedor">
<div class="card">
	<div class="card-content light-blue accent-4">
		<span class="card-title white-text center-align">Slider Principal</span>		
	</div>
	<div class="row">
	    <div class="col s12 m4">
			<div class="card">
		        <div class="card-image">
		          <img class="materialboxed" src="{{ asset('img/slider/slider_1.jpg') }}">
		          <a href="{{ url('panel/slider/1/editar') }}" class="btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">mode_edit</i></a>
		        </div>
	      	</div>
	    </div>

	    <div class="col s12 m4">
			<div class="card">
		        <div class="card-image">
		          <img class="materialboxed" src="{{ asset('img/slider/slider_2.jpg') }}">
		          <a href="{{ url('panel/slider/2/editar') }}" class="btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">mode_edit</i></a>
		        </div>
	      	</div>
	    </div>

	    <div class="col s12 m4">
			<div class="card">
		        <div class="card-image">
		          <img class="materialboxed" src="{{ asset('img/slider/slider_3.jpg') }}">
		          <a href="{{ url('panel/slider/3/editar') }}" class="btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">mode_edit</i></a>
		        </div>
	      	</div>
	    </div>
  	</div>
	
    <div class="divider"></div>
 </div>
 <br>
 <div class="fixed-action-btn">
    <a class="btn-floating btn-large light-blue accent-4" href="{{ url('panel/usuarios') }}">
      <i class="large material-icons green">swap_horiz</i>
    </a>
 </div>
</div>
@endsection

@section('script')
	@include('panel.partials.sweetSure')

	<script type="text/javascript">
		$(document).ready(function(){
	    	$('.materialboxed').materialbox();
	  	});
	</script>
@endsection