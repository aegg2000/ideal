@extends('layouts.main')

@section('activeZona')
	activePage
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
@endsection

@section('contenido')

	<div class="progress"><div class="indeterminate"></div></div>

	<div id="pagebody" class="row hide">		
		
	    <div id="tabs" class="col s12">
	      	<ul class="tabs">	      		
		        <li class="tab col s2"><a href="#ideal" class="active">Ideal</a></li>
	        	<li class="tab col s2"><a href="#preescolar">Preescolar</a></li>
		        <li class="tab col s2"><a href="#primaria" id="activeSearch">Primaria</a></li>
		        <li class="tab col s2"><a href="#bachillerato">Bachillerato</a></li>
		        <li class="tab col s3"><a href="#busqueda">
	      			{{ Form::open(['route' => 'zonaIdeal', 'method' => 'GET', 'class' => 'formBuscar']) }}			    						
						<div class="input-field" style="margin-top: 1px;">
							<input id="buscar" type="search" name="titulo" placeholder="Buscar" style="margin-bottom: 0;">
							<label class="label-icon" for="search"><i class="material-icons">search</i></label>
						</div>		
					{{ Form::close() }}
				</a></li>
	      	</ul>	
	    </div>

	    <div id="ideal" class="col s12">
	    	@include('partials.zonaBienvenido')
	    </div>

	    <div id="preescolar" class="col s12">
	   		<div class="row">
				 <div class="articulos">
					
				 	@include('partials.articulosZona', ['biblioteca' => $preescolar ])

				 </div>
			</div>
	    </div>

	    <div id="primaria" class="col s12">
	    	<div class="row">

		    	<ul class="collapsible col s3 nopadding" data-collapsible="accordion">
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Primer Grado</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[1] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Segundo Grado</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[2] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Tercer Grado</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[3] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Cuarto Grado</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[4] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Quinto Grado</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[5] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Sexto Grado</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[6] ])
					    </ul>
				      </div>
				    </li>
				 </ul>

				 <div class="col s9 articulos-primaria">
				 	<h3 class="centrado-mensaje">Seleccione una materia</h3>
				 </div>
			</div>
	    </div>

	    <div id="bachillerato" class="col s12 ">
	    	<div class="row">

		    	<ul class="collapsible col s3 nopadding" data-collapsible="accordion">
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Primer Año</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[7] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Segundo Año</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[8] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Tercer Año</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[9] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Cuarto Año</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[10] ])
					    </ul>
				      </div>
				    </li>
				    <li>
				      <div class="collapsible-header blue darken-2 white-text">Quinto Año</div>
				      <div class="collapsible-body collapsible-custom">
				      	<ul class="collection">
				      		@include('partials.materiasZona', ['materias' => $grado[11] ])
					    </ul>
				      </div>
				    </li>
				 </ul>

				 <div class="col s9 articulos-bachillerato">
				 	<h3 class="centrado-mensaje">Seleccione una materia</h3>
				 </div>
			</div>
		</div>

		<div id="busqueda" class="col s12">
			<h2 class="centrado-mensaje">Ingresa tu búsqueda...</h2>
	    </div>
  	</div>

  	<div id="myBtn" style="display: none;" class="fixed-action-btn back-to-top">
    	<a href="#"  class="btn-floating btn waves-effect waves-light blue "><i class="material-icons">arrow_upward</i></a>
    </div>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
    @include('panel.partials.errors')
	<script type="text/javascript">
		$(document).ready(function(){

    		$('.progress').remove();
    		$('#pagebody').removeClass('hide');

    		$('ul.tabs').tabs();

    		$('.collapsible').collapsible();

    		$('.formBuscar').on('submit', function(e) {
		      	e.preventDefault();
		      	var url = $(this).attr('action')+'?buscar='+$('#buscar').val();
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';	      	
		      	$('#busqueda').empty();
		      	$('#busqueda').html(loader);
		      	$.get(url, function(data){
		      		$('#busqueda').empty();
		      		$('#busqueda').html(data);
		      	});
	     	});

			$('.item-primaria').on('click', function(e) {
		      	e.preventDefault();
		      	$('.item-primaria.item-active').removeClass('item-active');
		      	$(this).addClass('item-active');
		      	var url = $(this).attr('href');
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';	      	
		      	$('.articulos-primaria').empty();
		      	$('.articulos-primaria').html(loader);
		      	$.get(url, function(data){
		      		$('.articulos-primaria').empty();
		      		$('.articulos-primaria').html(data);
		      	});
	     	}); 

	     	$('.item-bachillerato').on('click', function(e) {
		      	e.preventDefault();
		      	$('.item-bachillerato.item-active').removeClass('item-active');
		      	$(this).addClass('item-active');
		      	var url = $(this).attr('href');
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';	      	
		      	$('.articulos-bachillerato').empty();
		      	$('.articulos-bachillerato').html(loader);
		      	$.get(url, function(data){
		      		$('.articulos-bachillerato').empty();
		      		$('.articulos-bachillerato').html(data);
		      	});
	     	});

	     	$(document).on('click', '#vermas', function(e){
	     		console.log('vermas');
		      	e.preventDefault();
		      	var url = $(this).attr('href');
		      	var loader = '<div class="progress"><div class="indeterminate"></div></div>';
		      	$('#vermas').remove();
		      	$('#busqueda').append(loader);
		      	$.get(url, function(data){
		      		$('.progress').remove();
		      		$('#busqueda').append(data);
		      	});
	      	});

			$('.modal').modal();
    		$('select').material_select();

	     	var offset = 750;		 
			var duration = 300;
			var duration2 = 150;
			jQuery(window).scroll(function() {		 
				if (jQuery(this).scrollTop() > offset) {		 
					jQuery('.back-to-top').animate({height: 'show'});		 
				} else {		 
					jQuery('.back-to-top').fadeOut(duration2);		 
				}		 
			});	 		 
			jQuery('.back-to-top').click(function(event) {		 
				event.preventDefault();		 
				jQuery('html, body').animate({scrollTop: 0}, duration);		 
				return false;		 
			})
 		});

 		function enviarDatos(){
 			if($('#representante_name').val() == '' || $('#representante_ci').val() == '' || $('#representante_telf').val() == '' || $('#representante_email').val() == '' || $('alumno_name').val() == '' || $('alumno_ci').val() == '' || $('#grado').val() == '' || $('#motivo').val() == ''){
 				return;
 			}else{
 				Materialize.toast('Procesando su solicitud', 30000);	
 			} 			
 		} 		
	</script>
@endsection