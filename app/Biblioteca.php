<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biblioteca extends Model
{
    protected $table = 'bibliotecas';

    protected $fillable = ['titulo', 'contenido', 'enlace', 'nivel_id', 'materia_id', 'user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function materia(){
    	return $this->belongsTo('App\Materia');
    }

    public function nivel(){
    	return $this->belongsTo('App\Nivel');
    }

    public function scopeBuscar($query, $busqueda){
        return $query->where('titulo', 'LIKE', "%$busqueda%");
    }

    public function scopeFiltrar($query, $nivel, $materia){
        return $query->where([['nivel_id', 'LIKE', "%$nivel"], ['materia_id', 'LIKE', "%$materia"]]);
    }
}
