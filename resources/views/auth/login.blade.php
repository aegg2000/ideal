</!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Login | Panel de Administración</title>
	
	<!-- Iconos de Google -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Hojas de Estilos CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link rel="stylesheet" href="{{ asset('css/estilo.css') }}">
</head>	

<body style="background-color: #1127C6">    
    <div id="login-page" class="row">
        <div class="col s12 z-depth-4 card-panel">
          <form class="form-login" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            
            <div class="row">
              <div class="input-field col s12 center-align">
                <a href="{{ url('/') }}"><img src="{{ url('images/logologin/logo.png') }}" alt="" class="responsive-img valign"></a>
              </div>
            </div>
            <div class="row unrow">
              <div class="input-field col s12">
                <i class="material-icons prefix">person_outline</i>
                <input id="email" type="email" name="email" required>
                <label for="email" class="center-align">Correo Electronico</label>
              </div>
            </div>
            <div class="row unrow">
              <div class="input-field col s12">
                <i class="material-icons prefix">lock</i>
                <input id="password" type="password" name="password" required>
                <label for="password" class="">Contraseña</label>
              </div>
            </div>
            <div class="row">          
              <div class="col s12 m12 l12 login-text">
                  <input type="checkbox" id="remember-me" name="remember">
                  <label for="remember-me">Recuerdame</label>
              </div>
            </div>
            <div class="row unrow">
              <div class="input-field col s12">
                <button type="submit" class="customBtn btn waves-effect waves-light col s12">Iniciar Sesión</button>
              </div>
            </div>
            <div class="row">
              <div class="col s6 m6 l6">
              </div>
              <div class="input-field col s6 m6 l6">
                  <p class="margin right-align medium-small"><a href="{{ route('password.request') }}">Olvidé mi contraseña</a></p>
              </div>          
            </div>
          </form>
        </div>
      </div>

	<!-- scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert') 
    @include('panel.partials.errors')
    </body>
</html>