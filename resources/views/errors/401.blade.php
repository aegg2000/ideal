<!DOCTYPE html>
<html>
<head>
	<title>ERROR 401</title>
    <link rel="stylesheet" href="{{ asset('plugins/materialize/css/materialize.min.css') }}" media="screen,projection">
	<style type="text/css">
		body{
			background: #e0e0e0;
		}
		section{
			padding: 5%;
		}
		#mouse401{
			margin: 4% 0;
		}
		#div401{
			margin: 10% 0;
		}
	</style>
</head>
<body>
	<section class="center">
		<article class="card-panel">
			<div class="row">
				<img id="mouse401" class="col s4" src="{{ asset('img/401.png') }}">
				<div class="col s8" id="div401">
					<h3>¡Ups... Acceso no autorizado!</h1>
					<h5>Es posible que este intentando entrar a una página
					a la cual no tiene permisos.</h5>
				</div>
			</div></h3>
			<h4><a href="{{ route('panel.inicio') }}">Volver al panel</a></h4>
		</article>
	</section>
</body>
</html>